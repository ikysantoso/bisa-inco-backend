<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInputtedBySpecialNakesColumnInMasterMedicalCheckupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_medical_checkups', function (Blueprint $table) {
            $table->boolean('inputted_by_special_nakes')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_medical_checkups', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFewColumnsForAdjustmentCompanyInCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->text('companies_addr');
            $table->string('companies_phone', 13);
            $table->string('companies_fax', 13);
            $table->string('companies_email', 128);
            $table->string('companies_city', 128);
            $table->string('companies_province', 128);
            $table->string('companies_subdistrict', 128);
            $table->text('logos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            //
        });
    }
}

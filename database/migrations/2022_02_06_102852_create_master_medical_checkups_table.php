<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterMedicalCheckupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_medical_checkups', function (Blueprint $table) {
            $table->id();
            $table->foreignId('employee_id')->constrained()
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->integer('height');
            $table->integer('weight');
            $table->integer('sistole');
            $table->integer('diastole');
            $table->integer('blood_sugar_level');
            $table->integer('cholesterol_level');
            $table->double('uric_acid_level');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_medical_checkups');
    }
}

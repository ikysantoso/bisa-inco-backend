<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNakesSpecialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nakes_specials', function (Blueprint $table) {
            $table->id();
            $table->text('fullname');
            $table->mediumInteger('phone');
            $table->mediumInteger('nik');
            $table->text('instance_name');
            $table->mediumText('instance_address');
            $table->text('nursing_number');
            $table->bigInteger('company_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nakes_specials');
    }
}

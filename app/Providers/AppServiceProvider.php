<?php

namespace App\Providers;

use App\Repository\Authentication\AuthenticationRepository;
use App\Repository\Authentication\AuthenticationRepositoryInterface;
use App\Repository\Company\V1\CompanyRepository;
use App\Repository\Company\V1\CompanyRepositoryInterface;
use App\Repository\Employee\V1\EmployeeRepository;
use App\Repository\Employee\V1\EmployeeRepositoryInterface;
use App\Repository\MedicalCheckup\V1\MedicalCheckupRepository;
use App\Repository\MedicalCheckup\V1\MedicalCheckupRepositoryInterface;
use App\Repository\NakesSpecial\V1\NakesSpecialRepository;
use App\Repository\NakesSpecial\V1\NakesSpecialRepositoryInterface;
use App\Repository\Partnership\V1\PartnershipRepository;
use App\Repository\Partnership\V1\PartnershipRepositoryInterface;
use App\Repository\ResetPasswordRequest\V1\ResetPasswordRequestRepository;
use App\Repository\ResetPasswordRequest\V1\ResetPasswordRequestRepositoryInterface;
use App\Repository\User\V1\UserRepository;
use App\Repository\User\V1\UserRepositoryInterface;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthenticationRepositoryInterface::class, AuthenticationRepository::class);
        $this->app->bind(EmployeeRepositoryInterface::class, EmployeeRepository::class);
        $this->app->bind(CompanyRepositoryInterface::class, CompanyRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(MedicalCheckupRepositoryInterface::class, MedicalCheckupRepository::class);
        $this->app->bind(PartnershipRepositoryInterface::class, PartnershipRepository::class);
        $this->app->bind(NakesSpecialRepositoryInterface::class, NakesSpecialRepository::class);
        $this->app->bind(ResetPasswordRequestRepositoryInterface::class, ResetPasswordRequestRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Jakarta');
    }
}

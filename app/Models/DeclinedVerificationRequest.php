<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeclinedVerificationRequest extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = array('id');

    protected $fillable = [
        'request_id',
        'nik',
        'fullname',
        'birthdate',
        'division'
    ];
}

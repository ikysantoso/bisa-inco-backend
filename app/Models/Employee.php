<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = array('id');

    protected $fillable = [
        'company_id',
        'employee_id',
        'nik',
        'fullname',
        'birthdate',
        'division',
        'gender'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function masterMedicalCheckup()
    {
        return $this->hasOne(MasterMedicalCheckup::class);
    }
}

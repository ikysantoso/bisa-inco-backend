<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeDeleteRequest extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = array('id');

    protected $fillable = [
        'company_id',
        'request_id',
        'nik',
        'fullname',
        'birthdate',
        'division'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NakesSpecials extends Model
{
    use HasFactory;

    protected $guarded = array('id');

    protected $fillable = [
        'company_id',
        'nursing_number',
        'fullname',
        'nik',
        'phone',
        'instance_name',
        'instance_address'
    ];
}

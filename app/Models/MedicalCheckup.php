<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedicalCheckup extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = array('id');

    protected $fillable = [
        'employee_id',
        'height',
        'weight',
        'sistole',
        'diastole',
        'blood_sugar_level',
        'cholesterol_level',
        'uric_acid_level',
        'checkup_date',
        'nursing_number',
        'inputted_by_special_nakes'
    ];

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}

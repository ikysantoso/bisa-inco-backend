<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterMedicalCheckup extends Model
{
    use HasFactory;

    protected $guarded = array('id');

    protected $fillable = [
        'employee_id',
        'height',
        'weight',
        'sistole',
        'diastole',
        'blood_sugar_level',
        'cholesterol_level',
        'uric_acid_level',
        'checkup_date',
        'nursing_number',
        'inputted_by_special_nakes'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}

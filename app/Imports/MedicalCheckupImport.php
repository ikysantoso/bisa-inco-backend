<?php

namespace App\Imports;

use App\Models\Employee;
use App\Models\MedicalCheckup;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class MedicalCheckupImport implements ToModel, WithStartRow
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
    * @param array $row
    *
    * @return Model|null
    */
    public function model(array $row)
    {
        $employee = Employee::where('nik', $row[7])->first();

        if (isset($employee)) {
            return new MedicalCheckup([
                "employee_id"               => $employee->id,
                "height"                    => $row[9],
                "weight"                    => $row[8],
                "sistole"                   => $row[10],
                "diastole"                  => $row[11],
                "blood_sugar_level"         => $row[12],
                "cholesterol_level"         => $row[13],
                "uric_acid_level"           => $row[14],
            ]);
        }
    }
}

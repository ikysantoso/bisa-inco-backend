<?php

namespace App\Repository\MedicalCheckup\V1;

use App\Http\Resources\V1\MedicalCheckupResource;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Fitness;
use App\Models\MasterMedicalCheckup;
use App\Models\MedicalCheckup;
use App\Models\NakesSpecials;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;

class MedicalCheckupRepository implements MedicalCheckupRepositoryInterface
{
    protected $importedMedicalCheckupsData = array();
    protected $declinedMedicalCheckupsData = array();

    public function getAll(): AnonymousResourceCollection
    {
        $medicalCheckups = MedicalCheckup::with('employee')->get();

        return MedicalCheckupResource::collection($medicalCheckups);
    }

    public function getById($id): ?MedicalCheckupResource
    {
        $medicalCheckup = MedicalCheckup::find($id);

        if ($medicalCheckup) {
            return new MedicalCheckupResource($medicalCheckup->with('employee'));
        } else {
            return null;
        }
    }

    public function create($request): ?MedicalCheckupResource
    {
        $employee = Employee::where('nik', $request->nik)->first();

        if ($employee) {
            $input['employee_id'] = $employee->id;
            $input['height'] = $request->height;
            $input['weight'] = $request->weight;
            $input['sistole'] = $request->sistole;
            $input['diastole'] = $request->diastole;
            $input['blood_sugar_level'] = $request->bloodSugarLevel;
            $input['cholesterol_level'] = $request->cholesterolLevel;
            $input['uric_acid_level'] = $request->uricAcidLevel;
            $input['checkup_date'] = $request->checkupDate;

            if (isset($request->nursingNumber) && $request->nursingNumber > 0) {
                $nakes_special = NakesSpecials::where('nursing_number', '=', $request->nursingNumber)
                    ->where('company_id', '=', $employee->company_id)
                    ->first();

                if (isset($nakes_special)) {
                    $input['nursing_number'] = $request->nursingNumber;
                    $input['inputted_by_special_nakes'] = true;

                    $employee->masterMedicalCheckup->nursing_number = $request->nursingNumber;
                    $employee->masterMedicalCheckup->inputted_by_special_nakes = true;
                } else {
                    $url = env('BISA_NAKES_BASE_API') . '/team/' . $request->nursingNumber;
                    $client = new \GuzzleHttp\Client();
                    $response = $client->request('GET', $url);

                    $exist_nakes = json_decode($response->getBody(), true);

                    if (isset($exist_nakes)) {
                        $input['nursing_number'] = $request->nursingNumber;
                        $input['inputted_by_special_nakes'] = false;

                        $employee->masterMedicalCheckup->nursing_number = $request->nursingNumber;
                        $employee->masterMedicalCheckup->inputted_by_special_nakes = false;
                    } else {
                        $employee->masterMedicalCheckup->nursing_number = null;
                        $employee->masterMedicalCheckup->inputted_by_special_nakes = false;
                    }
                }
            } else {
                $employee->masterMedicalCheckup->nursing_number = null;
                $employee->masterMedicalCheckup->inputted_by_special_nakes = false;
            }

            $medicalCheckup = MedicalCheckup::create($input);

            $employee->masterMedicalCheckup->height = $medicalCheckup->height;
            $employee->masterMedicalCheckup->weight = $medicalCheckup->weight;
            $employee->masterMedicalCheckup->sistole = $medicalCheckup->sistole;
            $employee->masterMedicalCheckup->diastole = $medicalCheckup->diastole;
            $employee->masterMedicalCheckup->blood_sugar_level = $medicalCheckup->blood_sugar_level;
            $employee->masterMedicalCheckup->cholesterol_level = $medicalCheckup->cholesterol_level;
            $employee->masterMedicalCheckup->uric_acid_level = $medicalCheckup->uric_acid_level;
            $employee->masterMedicalCheckup->checkup_date = $medicalCheckup->checkup_date;

            $employee->masterMedicalCheckup->save();

            return new MedicalCheckupResource($medicalCheckup);
        } else {
            return null;
        }
    }

    public function update($request, $id)
    {
        //
    }

    public function delete($id): object
    {
        $medicalCheckup = MedicalCheckup::find($id);

        if ($medicalCheckup) {
            return $medicalCheckup->delete();
        }

        return (object)[];
    }

    public function getAllMasterMedicalCheckup()
    {
        return MasterMedicalCheckup::with('employee')
                                    ->get();
    }

    public function getAllMasterMedicalCheckupByCompanyId($companyId): ?array
    {
        $company = Company::find($companyId);

        if ($company) {
            $medicalCheckups = MasterMedicalCheckup::whereHas('employee', function ($query) use ($companyId) {
                return $query->where('company_id', $companyId);
            })->with('employee')->get();

            $result = [];

            foreach ($medicalCheckups as $keyMedicalCheckup => $medicalCheckup) {
                $employee = Employee::find($medicalCheckup->employee->id);

                if (isset($employee)) {
                    $fitness = Fitness::where('employee_id', $employee->id)->first();

                    $adjustedMedicalCheckup = (object) [
                        'id'                    => $medicalCheckup->id,
                        'employee'              => $employee,
                        'bmi'                   => "-",
                        'hypertension'          => "-",
                        'blood_sugar_level'     => "-",
                        'cholesterol_level'     => "-",
                        'uric_acid_level'       => "-",
                        'dangerScoreDiagnose'   => 0,
                        'healthStatus'          => "Bugar",
                        'age'                   => 0,
                        'result'                => 0,
                        'nursing_number'                   => $medicalCheckup->nursing_number,
                        'inputted_by_special_nakes'        => $medicalCheckup->inputted_by_special_nakes,
                    ];

                    if ($medicalCheckup->height && $medicalCheckup->weight > 0) {
                        $bmi = round($medicalCheckup->weight/(pow(($medicalCheckup->height/100),2)),2);

                        if ($bmi > 0 && $bmi < 18.5) {
                            $adjustedMedicalCheckup->bmi = $bmi . " - Underweight";
                            $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                        } else if($bmi >= 18.5 && $bmi < 23) {
                            $adjustedMedicalCheckup->bmi = $bmi . " - Normal";
                        } else if ($bmi >= 23 && $bmi < 25) {
                            $adjustedMedicalCheckup->bmi = $bmi . " - Overweight";
                            $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                        } else if ($bmi >= 25) {
                            $adjustedMedicalCheckup->bmi = $bmi . " - Obese";
                            $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                        }
                    }

                    if ($medicalCheckup->sistole > 80 && $medicalCheckup->sistole < 140) {
                        if ($medicalCheckup->diastole > 0 && $medicalCheckup->diastole < 90) {
                            $adjustedMedicalCheckup->hypertension = $medicalCheckup->sistole . '/' . $medicalCheckup->diastole . ' - Normal';
                        } else if ($medicalCheckup->diastole >= 90 && $medicalCheckup->diastole < 100) {
                            $adjustedMedicalCheckup->hypertension = $medicalCheckup->sistole . '/' . $medicalCheckup->diastole . ' - Hipertensi Stage 1';
                            $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                        } else if ($medicalCheckup->diastole >= 100) {
                            $adjustedMedicalCheckup->hypertension = $medicalCheckup->sistole . '/' . $medicalCheckup->diastole . ' - Hipertensi Stage 2';
                            $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                        } else {
                            $adjustedMedicalCheckup->hypertension = $medicalCheckup->sistole . '/' . $medicalCheckup->diastole . ' - Tidak normal';
                        }
                    } else if ($medicalCheckup->sistole >= 140 && $medicalCheckup->sistole < 160) {
                        if ($medicalCheckup->diastole >= 100) {
                            $adjustedMedicalCheckup->hypertension = $medicalCheckup->sistole . '/' . $medicalCheckup->diastole . ' - Hipertensi Stage 2';
                            $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                        } else {
                            $adjustedMedicalCheckup->hypertension = $medicalCheckup->sistole . '/' . $medicalCheckup->diastole . ' - Hipertensi Stage 1';
                            $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                        }
                    } else if ($medicalCheckup->sistole >= 160) {
                        $adjustedMedicalCheckup->hypertension = $medicalCheckup->sistole . '/' . $medicalCheckup->diastole . ' - Hipertensi Stage 2';
                        $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                    } else {
                        $adjustedMedicalCheckup->hypertension = $medicalCheckup->sistole . '/' . $medicalCheckup->diastole . ' - Tidak normal';
                    }

                    if ($medicalCheckup->blood_sugar_level >= 71 && $medicalCheckup->blood_sugar_level < 140 ){
                        $adjustedMedicalCheckup->blood_sugar_level = $medicalCheckup->blood_sugar_level . ' - Normal';
                    } else if ($medicalCheckup->blood_sugar_level  >= 140) {
                        $adjustedMedicalCheckup->blood_sugar_level = $medicalCheckup->blood_sugar_level . ' - Hati-hati';
                        $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                    } else {
                        $adjustedMedicalCheckup->blood_sugar_level = $medicalCheckup->blood_sugar_level . ' - Tidak normal';
                    }

                    if ($medicalCheckup->cholesterol_level > 0 && $medicalCheckup->cholesterol_level < 240) {
                        $adjustedMedicalCheckup->cholesterol_level = $medicalCheckup->cholesterol_level . ' - Normal';
                    } else if($medicalCheckup->cholesterol_level >= 240) {
                        $adjustedMedicalCheckup->cholesterol_level = $medicalCheckup->cholesterol_level . ' - Hati-hati';
                        $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                    } else {
                        $adjustedMedicalCheckup->cholesterol_level = $medicalCheckup->cholesterol_level . ' - Tidak normal';
                    }

                    if ($employee->gender == 'Perempuan') {
                        if ($medicalCheckup->uric_acid_level > 0 && $medicalCheckup->uric_acid_level <= 5.7) {
                            $adjustedMedicalCheckup->uric_acid_level = $medicalCheckup->uric_acid_level . ' - Normal';
                        } else if ($medicalCheckup->uric_acid_level > 5.7) {
                            $adjustedMedicalCheckup->uric_acid_level = $medicalCheckup->uric_acid_level . ' - Hati-hati';
                            $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                        } else {
                            $adjustedMedicalCheckup->uric_acid_level = $medicalCheckup->uric_acid_level . ' - Tidak normal';
                        }
                    } else if ($employee->gender == 'Laki-Laki') {
                        if($medicalCheckup->uric_acid_level > 0 && $medicalCheckup->uric_acid_level <= 7) {
                            $adjustedMedicalCheckup->uric_acid_level = $medicalCheckup->uric_acid_level . ' - Normal';
                        } else if($medicalCheckup->uric_acid_level > 7 && $medicalCheckup->uric_acid_level < 15) {
                            $adjustedMedicalCheckup->uric_acid_level = $medicalCheckup->uric_acid_level . ' - Hati-hati';
                            $adjustedMedicalCheckup->dangerScoreDiagnose = $adjustedMedicalCheckup->dangerScoreDiagnose + 1;
                        } else {
                            $adjustedMedicalCheckup->uric_acid_level = $medicalCheckup->uric_acid_level . ' - Tidak normal';
                        }
                    }

                    if (isset($fitness)) {
                        $birthdate = Carbon::createFromFormat('Y-m-d', $employee->birthdate);
                        $now = Carbon::now();
                        $age = $birthdate->diffInYears($now);

                        if ($employee->gender == "Perempuan") {
                            $height_counter = 2.11 * $fitness->height;
                            $age_counter = 5.78 * $age;
                            $weight_counter = 2.29 * $fitness->weight;

                            $resultFitness = $height_counter - $age_counter - $weight_counter + 667;

                            if ($fitness->mileage < $resultFitness) {
                                $adjustedMedicalCheckup->healthStatus = "Tidak Bugar";
                            }
                        } else if ($employee->gender == "Laki-Laki") {
                            $height_counter = 7.57 * $fitness->height;
                            $age_counter = 5.02 * $age;
                            $weight_counter = 1.76 * $fitness->weight;

                            $resultFitness = $height_counter - $age_counter - $weight_counter - 309;

                            if ($fitness->mileage < $resultFitness) {
                                $adjustedMedicalCheckup->healthStatus = "Tidak Bugar";
                            }
                        }
                    }

                    $result[] = $adjustedMedicalCheckup;
                } else {
                    return null;
                }
            }

            return $result;
        } else {
            return null;
        }
    }

    public function deleteMasterMedicalCheckup($id)
    {
        $masterMedicalCheckup = MasterMedicalCheckup::find($id);

        $masterMedicalCheckup->delete();

        return $masterMedicalCheckup;
    }

    public function getPercentageMedicalCheckup($companyId, $specific_diagnose, $diagnose): ?object
    {
        $company = Company::find($companyId);

        if ($company) {
            $master_medical_checkups = DB::table('master_medical_checkups')
                ->join('employees', 'master_medical_checkups.employee_id', '=', 'employees.id')
                ->where('employees.company_id', '=', $companyId)
                ->get()
                ->toArray();

            $underweight = 0;
            $overweight = 0;
            $obese = 0;
            $stageOneHypertension = 0;
            $stageTwoHypertension = 0;
            $bloodSugar = 0;
            $uricAcid = 0;
            $hyperCholesterol = 0;

            $healthy = 0;
            $dangerScoreDiagnose = 0;

            $zeroDangerScoreDiagnose = 0;
            $oneDangerScoreDiagnose = 0;
            $twoDangerScoreDiagnose = 0;
            $threeDangerScoreDiagnose = 0;
            $fourDangerScoreDiagnose = 0;
            $fiveDangerScoreDiagnose = 0;

            $notHealthy = 0;

            $stageOneHypertensionDetected = [];
            $stageTwoHypertensionDetected = [];
            $bloodSugarDetected = [];
            $hyperCholesterolDetected = [];
            $uricAcidDetected = [];
            $underweightDetected = [];
            $overweightDetected = [];
            $obeseDetected = [];
            $healthyDetected = [];
            $dangerScoreDiagnoseDetected = [];
            $oneDangerScoreDiagnoseDetected = [];
            $twoDangerScoreDiagnoseDetected = [];
            $threeDangerScoreDiagnoseDetected = [];
            $fourDangerScoreDiagnoseDetected = [];
            $fiveDangerScoreDiagnoseDetected = [];
            $zeroDangerScoreDiagnoseDetected = [];
            $notHealthyDetected = [];

            foreach ($master_medical_checkups as $medicalCheckups => $checkup) {
                $notHealthyPerEmployee = 0;
                $employeeSelfHealthDiagnose = 0;

                $employee = Employee::where('employee_id', $checkup->employee_id)->first();

                if (isset($employee)) {
                    $fitness = Fitness::where('employee_id', $employee->id)->first();

                    if (isset($fitness)) {
                        if ($employee->gender == "Perempuan") {
                            $birthdate = Carbon::createFromFormat('Y-m-d', $employee->birthdate);
                            $now = Carbon::now();
                            $age = $birthdate->diffInYears($now);

                            $height_counter = 2.11 * $fitness->height;
                            $age_counter = 5.78 * $age;
                            $weight_counter = 2.29 * $fitness->weight;

                            $result = $height_counter - $age_counter - $weight_counter + 667;

                            if ($fitness->mileage < $result) {
                                $notHealthyPerEmployee = $notHealthyPerEmployee + 1;
                                $notHealthy = $notHealthy + 1;

                                $notHealthyDetected[] = $employee;
                            }
                        } else if ($employee->gender == "Laki-Laki") {
                            $birthdate = Carbon::createFromFormat('Y-m-d', $employee->birthdate);
                            $now = Carbon::now();
                            $age = $birthdate->diffInYears($now);

                            $height_counter = 7.57 * $fitness->height;
                            $age_counter = 5.02 * $age;
                            $weight_counter = 1.76 * $fitness->weight;

                            $result = $height_counter - $age_counter - $weight_counter - 309;

                            if ($fitness->mileage < $result) {
                                $notHealthyPerEmployee = $notHealthyPerEmployee + 1;
                                $notHealthy = $notHealthy + 1;

                                $notHealthyDetected[] = $employee;
                            }
                        }
                    }

                    if ($checkup->height && $checkup->weight > 0) {
                        $bmi = round($checkup->weight / (pow(($checkup->height / 100), 2)), 2);

                        if ($bmi > 0 && $bmi < 18.5) {
                            $underweight = $underweight + 1;

                            $underweightDetected[] = $employee;
                        } else if ($bmi >= 23 && $bmi < 25) {
                            $overweight = $overweight + 1;
                            $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                            $overweightDetected[] = $employee;
                        } else if ($bmi >= 25) {
                            $obese = $obese + 1;
                            $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                            $obeseDetected[] = $employee;
                        }
                    }

                    if ($checkup->sistole > 80 && $checkup->sistole < 140) {
                        if ($checkup->diastole >= 90 && $checkup->diastole < 100) {
                            $stageOneHypertension = $stageOneHypertension + 1;
                            $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                            $stageOneHypertensionDetected[] = $employee;
                        } else if ($checkup->diastole >= 100) {
                            $stageTwoHypertension = $stageTwoHypertension + 1;
                            $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                            $stageTwoHypertensionDetected[] = $employee;
                        }
                    } else if ($checkup->sistole >= 140 && $checkup->sistole < 160) {
                        if ($checkup->diastole >= 100) {
                            $stageTwoHypertension = $stageTwoHypertension + 1;
                            $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                            $stageTwoHypertensionDetected[] = $employee;
                        } else {
                            $stageOneHypertension = $stageOneHypertension + 1;
                            $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                            $stageOneHypertensionDetected[] = $employee;
                        }
                    } else if ($checkup->sistole >= 160) {
                        $stageTwoHypertension = $stageTwoHypertension + 1;
                        $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                        $stageTwoHypertensionDetected[] = $employee;
                    }

                    if ($checkup->blood_sugar_level >= 140) {
                        $bloodSugar = $bloodSugar + 1;
                        $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                        $bloodSugarDetected[] = $employee;
                    }

                    if ($employee->gender == "Perempuan") {
                        if ($checkup->uric_acid_level > 5.7) {
                            $uricAcid = $uricAcid + 1;
                            $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                            $uricAcidDetected[] = $employee;
                        }
                    } else if ($employee->gender == "Laki-Laki") {
                        if ($checkup->uric_acid_level > 7) {
                            $uricAcid = $uricAcid + 1;
                            $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                            $uricAcidDetected[] = $employee;
                        }
                    }

                    if ($checkup->cholesterol_level >= 240) {
                        $hyperCholesterol = $hyperCholesterol + 1;
                        $employeeSelfHealthDiagnose = $employeeSelfHealthDiagnose + 1;

                        $hyperCholesterolDetected[] = $employee;
                    }

                    if ($employeeSelfHealthDiagnose == 0) {
                        if ($notHealthyPerEmployee == 0) {
                            $healthy = $healthy + 1;

                            $healthyDetected[] = $employee;
                        } else {
                            $zeroDangerScoreDiagnose = $zeroDangerScoreDiagnose + 1;
                            $zeroDangerScoreDiagnoseDetected[] = $employee;
                        }
                    } else if ($employeeSelfHealthDiagnose == 1) {
                        $oneDangerScoreDiagnose = $oneDangerScoreDiagnose + 1;

                        $oneDangerScoreDiagnoseDetected[] = $employee;
                    } else if ($employeeSelfHealthDiagnose == 2) {
                        $twoDangerScoreDiagnose = $twoDangerScoreDiagnose + 1;

                        $twoDangerScoreDiagnoseDetected[] = $employee;
                    } else if ($employeeSelfHealthDiagnose == 3) {
                        $threeDangerScoreDiagnose = $threeDangerScoreDiagnose + 1;
                        $dangerScoreDiagnose = $dangerScoreDiagnose + 1;

                        $dangerScoreDiagnoseDetected[] = $employee;
                        $threeDangerScoreDiagnoseDetected[] = $employee;
                    } else if ($employeeSelfHealthDiagnose == 4) {
                        $fourDangerScoreDiagnose = $fourDangerScoreDiagnose + 1;
                        $dangerScoreDiagnose = $dangerScoreDiagnose + 1;

                        $dangerScoreDiagnoseDetected[] = $employee;
                        $fourDangerScoreDiagnoseDetected[] = $employee;
                    } else if ($employeeSelfHealthDiagnose == 5) {
                        $fiveDangerScoreDiagnose = $fiveDangerScoreDiagnose + 1;
                        $dangerScoreDiagnose = $dangerScoreDiagnose + 1;

                        $dangerScoreDiagnoseDetected[] = $employee;
                        $fiveDangerScoreDiagnoseDetected[] = $employee;
                    }
                }
            }

            if ($specific_diagnose) {
                switch ($diagnose) {
                    case "stageOneHypertension":
                        return (object)[
                            'counter' => $stageOneHypertension,
                            'employee' => $stageOneHypertensionDetected
                        ];

                    case "stageTwoHypertension":
                        return (object)[
                            'counter' => $stageTwoHypertension,
                            'employee' => $stageTwoHypertensionDetected
                        ];

                    case "bloodSugar":
                        return (object)[
                            'counter' => $bloodSugar,
                            'employee' => $bloodSugarDetected
                        ];

                    case "cholesterol":
                        return (object)[
                            'counter' => $hyperCholesterol,
                            'employee' => $hyperCholesterolDetected
                        ];

                    case "uricAcid":
                        return (object)[
                            'counter' => $uricAcid,
                            'employee' => $uricAcidDetected
                        ];

                    case "underweight":
                        return (object)[
                            'counter' => $underweight,
                            'employee' => $underweightDetected
                        ];

                    case "overweight":
                        return (object)[
                            'counter' => $overweight,
                            'employee' => $overweightDetected
                        ];

                    case "obese":
                        return (object)[
                            'counter' => $obese,
                            'employee' => $obeseDetected
                        ];

                    case "healthy":
                        return (object)[
                            'counter' => $healthy,
                            'employee' => $healthyDetected
                        ];

                    case "dangerScoreDiagnose":
                        return (object)[
                            'counter' => $dangerScoreDiagnose,
                            'employee' => $dangerScoreDiagnoseDetected
                        ];

                    case "oneDangerScoreDiagnose":
                        return (object)[
                            'counter' => $oneDangerScoreDiagnose,
                            'employee' => $oneDangerScoreDiagnoseDetected
                        ];

                    case "twoDangerScoreDiagnose":
                        return (object)[
                            'counter' => $twoDangerScoreDiagnose,
                            'employee' => $twoDangerScoreDiagnoseDetected
                        ];

                    case "threeDangerScoreDiagnose":
                        return (object)[
                            'counter' => $threeDangerScoreDiagnose,
                            'employee' => $threeDangerScoreDiagnoseDetected
                        ];

                    case "fourDangerScoreDiagnose":
                        return (object)[
                            'counter' => $fourDangerScoreDiagnose,
                            'employee' => $fourDangerScoreDiagnoseDetected
                        ];

                    case "fiveDangerScoreDiagnose":
                        return (object)[
                            'counter' => $fiveDangerScoreDiagnose,
                            'employee' => $fiveDangerScoreDiagnoseDetected
                        ];

                    case "zeroDangerScoreDiagnose":
                        return (object)[
                            'counter' => $zeroDangerScoreDiagnose,
                            'employee' => $zeroDangerScoreDiagnoseDetected
                        ];

                    case "notHealthy":
                        return (object)[
                            'counter' => $notHealthy,
                            'employee' => $notHealthyDetected
                        ];

                    default:
                        return (object)[];
                }
            } else {
                return (object)[
                    'stageOneHypertension' => (object)[
                        'counter' => $stageOneHypertension,
                        'employee' => $stageOneHypertensionDetected
                    ],
                    'stageTwoHypertension' => (object)[
                        'counter' => $stageTwoHypertension,
                        'employee' => $stageTwoHypertensionDetected
                    ],
                    'bloodSugar' => (object)[
                        'counter' => $bloodSugar,
                        'employee' => $bloodSugarDetected
                    ],
                    'cholesterol' => (object)[
                        'counter' => $hyperCholesterol,
                        'employee' => $hyperCholesterolDetected
                    ],
                    'uricAcid' => (object)[
                        'counter' => $uricAcid,
                        'employee' => $uricAcidDetected
                    ],
                    'underweight' => (object)[
                        'counter' => $underweight,
                        'employee' => $underweightDetected
                    ],
                    'overweight' => (object)[
                        'counter' => $overweight,
                        'employee' => $overweightDetected
                    ],
                    'obese' => (object)[
                        'counter' => $obese,
                        'employee' => $obeseDetected
                    ],
                    'healthy' => (object)[
                        'counter' => $healthy,
                        'employee' => $healthyDetected
                    ],
                    'dangerScoreDiagnose' => (object)[
                        'counter' => $dangerScoreDiagnose,
                        'employee' => $dangerScoreDiagnoseDetected
                    ],
                    'oneDangerScoreDiagnose' => (object)[
                        'counter' => $oneDangerScoreDiagnose,
                        'employee' => $oneDangerScoreDiagnoseDetected
                    ],
                    'twoDangerScoreDiagnose' => (object)[
                        'counter' => $twoDangerScoreDiagnose,
                        'employee' => $twoDangerScoreDiagnoseDetected
                    ],
                    'threeDangerScoreDiagnose' => (object)[
                        'counter' => $threeDangerScoreDiagnose,
                        'employee' => $threeDangerScoreDiagnoseDetected
                    ],
                    'fourDangerScoreDiagnose' => (object)[
                        'counter' => $fourDangerScoreDiagnose,
                        'employee' => $fourDangerScoreDiagnoseDetected
                    ],
                    'fiveDangerScoreDiagnose' => (object)[
                        'counter' => $fiveDangerScoreDiagnose,
                        'employee' => $fiveDangerScoreDiagnoseDetected
                    ],
                    'zeroDangerScoreDiagnose' => (object)[
                        'counter' => $zeroDangerScoreDiagnose,
                        'employee' => $zeroDangerScoreDiagnoseDetected
                    ],
                    'notHealthy' => (object)[
                        'counter' => $notHealthy,
                        'employee' => $notHealthyDetected
                    ]
                ];
            }
        }

        return null;
    }

    public function getMedicalCheckupByStatus($status, $companyId)
    {
        switch($status) {
            case 'stageOneHypertension':
                return $this->getHealthDiagnose('stageOneHypertension', $companyId);
            case 'stageTwoHypertension':
                return $this->getHealthDiagnose('stageTwoHypertension', $companyId);
            case 'bloodSugar':
                return $this->getHealthDiagnose('bloodSugar', $companyId);
            case 'cholesterol':
                return $this->getHealthDiagnose('cholesterol', $companyId);
            case 'uricAcid':
                return $this->getHealthDiagnose('uricAcid', $companyId);
            default:
                return null;
        }
    }

    private function getHealthDiagnose($diagnose, $companyId)
    {
        switch($diagnose) {
            case 'stageOneHypertension':
                return Employee::whereHas('masterMedicalCheckup', function ($query) {
                    $query->where(function($q) {
                        $q->where('sistole', '<', 140)->where('diastole', '<', 100);
                    });
                })->with('masterMedicalCheckup')->where('company_id', $companyId)->get();

            case 'stageTwoHypertension':
                return Employee::whereHas('masterMedicalCheckup', function ($query) {
                    $query->where(function($q) {
                        $q->where('sistole', '>=', 140)->where('diastole', '>=', 100);
                    });
                })->with('masterMedicalCheckup')->where('company_id', $companyId)->get();

            case 'bloodSugar':
                return Employee::whereHas('masterMedicalCheckup', function ($query) use ($companyId) {
                    $query->where(function ($q) {
                        $q->where('blood_sugar_level', '<', 71);
                    })->orWhere(function ($q) {
                        $q->where('blood_sugar_level', '>=', 140);
                    });
                })->with('masterMedicalCheckup')->where('company_id', $companyId)->get();


            case 'cholesterol':
                return Employee::whereHas('masterMedicalCheckup', function ($query) use ($companyId) {
                    $query->where(function($q) {
                        $q->where('cholesterol_level', '<', 0);
                    })->orWhere(function($q) {
                        $q->where('cholesterol_level', '>=', 240);
                    });
                })->with('masterMedicalCheckup')->where('company_id', $companyId)->get();

            case 'uricAcid':
                return Employee::whereHas('masterMedicalCheckup', function ($query) {
                    $query->where(function($q) {
                        $q->where('uric_acid_level', '<', 0);
                    })->orWhere(function($q) {
                        $q->where('uric_acid_level', '>=', 7);
                    });
                })->with('masterMedicalCheckup')->where('company_id', $companyId)->get();

            case 'underweight':
                return Employee::whereHas('masterMedicalCheckup', function ($query) {
                    $query->where(function($q) {
                        $q->where('height', '>', 0)->where('weight', '<', 0);
                    });
                })->with('masterMedicalCheckup')->where('company_id', $companyId)->get();

            default:
                return null;
        }
    }

    public function getDiagnoseTotalMedicalCheckup($companyId): ?object
    {
        $company = Company::find($companyId);

        if ($company) {
            return (object) [
                'employee'    => $this->getEmployeeTotal($companyId),
                'cholesterol' => $this->getCholesterolTotal($companyId),
                'bloodSugar'  => $this->getBloodSugarTotal($companyId),
                'uricAcid'    => $this->getUricAcidTotal($companyId),
                'hypertension' => $this->getHypertensionTotal($companyId),
                'bodyMassIndex'  => $this->getBodyMaxIndexTotal($companyId)
            ];
        } else {
            return null;
        }
    }

    private function getEmployeeTotal($companyId): array
    {
        $employeeData = Employee::select('id')
                                ->where('company_id', $companyId)
                                ->get()
                                ->groupBy(function($date) {
                                    return Carbon::parse($date->created_at)->format('m');
                                });

        return $this->generatePeriodData($employeeData);
    }

    private function getCholesterolTotal($companyId): array
    {
        $cholesterolData = MasterMedicalCheckup::join('employees', 'employees.id', '=', 'master_medical_checkups.employee_id')
                                               ->where('employees.company_id', $companyId)
                                               ->where(function ($query) {
                                                   $query->where('master_medical_checkups.cholesterol_level', '<', 0)
                                                       ->orWhere('master_medical_checkups.cholesterol_level', '>=', 240);
                                               })
                                               ->get()
                                               ->groupBy(function($date) {
                                                   return Carbon::parse($date->created_at)->format('m');
                                               });

        $normalData = MasterMedicalCheckup::join('employees', 'employees.id', '=', 'master_medical_checkups.employee_id')
                                            ->where('employees.company_id', $companyId)
                                            ->where(function ($query) {
                                                $query->where('master_medical_checkups.cholesterol_level', '>', 0)
                                                    ->orWhere('master_medical_checkups.cholesterol_level', '<', 240);
                                            })
                                            ->get()
                                            ->groupBy(function($date) {
                                                return Carbon::parse($date->created_at)->format('m');
                                            });

        $periodCounter = [];
        $normalDataPeriodCounter = [];

        foreach ($cholesterolData as $key => $value) {
            $periodCounter[(int)$key] = count($value);
        }

        foreach ($normalData as $key => $value) {
            $normalDataPeriodCounter[(int)$key] = count($value);
        }

        $finalData = [];

        for($i = 1; $i <= 12; $i++){
            if(!empty($periodCounter[$i]) && !empty($normalDataPeriodCounter[$i])){
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => $normalDataPeriodCounter[$i],
                    'cholesterol' => $periodCounter[$i]
                ];
            } else if (empty($periodCounter[$i]) && !empty($normalDataPeriodCounter[$i])) {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => $normalDataPeriodCounter[$i],
                    'cholesterol' => 0
                ];
            } else if (!empty($periodCounter[$i]) && empty($normalDataPeriodCounter[$i])) {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => 0,
                    'cholesterol' => $periodCounter[$i]
                ];
            } else {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => 0,
                    'cholesterol' => 0
                ];
            }
        }

        return $finalData;
    }

    private function getBloodSugarTotal($companyId): array
    {
        $bloodSugarData = MasterMedicalCheckup::join('employees', 'employees.id', '=', 'master_medical_checkups.employee_id')
                                                ->where('employees.company_id', $companyId)
                                                ->where(function ($query) {
                                                    $query->where('master_medical_checkups.blood_sugar_level', '<', 71)
                                                        ->orWhere('master_medical_checkups.blood_sugar_level', '>=', 140);
                                                })
                                                ->get()
                                                ->groupBy(function($date) {
                                                    return Carbon::parse($date->created_at)->format('m');
                                                });

        $normalData = MasterMedicalCheckup::join('employees', 'employees.id', '=', 'master_medical_checkups.employee_id')
                                            ->where('employees.company_id', $companyId)
                                            ->where(function ($query) {
                                                $query->where('master_medical_checkups.blood_sugar_level', '>', 71)
                                                    ->orWhere('master_medical_checkups.blood_sugar_level', '<', 140);
                                            })
                                            ->get()
                                            ->groupBy(function($date) {
                                                return Carbon::parse($date->created_at)->format('m');
                                            });

        $periodCounter = [];
        $normalDataPeriodCounter = [];

        foreach ($bloodSugarData as $key => $value) {
            $periodCounter[(int)$key] = count($value);
        }

        foreach ($normalData as $key => $value) {
            $normalDataPeriodCounter[(int)$key] = count($value);
        }

        $finalData = [];

        for($i = 1; $i <= 12; $i++){
            if(!empty($periodCounter[$i]) && !empty($normalDataPeriodCounter[$i])){
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => $normalDataPeriodCounter[$i],
                    'bloodSugar' => $periodCounter[$i]
                ];
            } else if (empty($periodCounter[$i]) && !empty($normalDataPeriodCounter[$i])) {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => $normalDataPeriodCounter[$i],
                    'bloodSugar' => 0
                ];
            } else if (!empty($periodCounter[$i]) && empty($normalDataPeriodCounter[$i])) {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => 0,
                    'bloodSugar' => $periodCounter[$i]
                ];
            } else {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => 0,
                    'bloodSugar' => 0
                ];
            }
        }

        return $finalData;
    }

    private function getUricAcidTotal($companyId): array
    {
        $uricAcidData = MasterMedicalCheckup::join('employees', 'employees.id', '=', 'master_medical_checkups.employee_id')
                                            ->where('employees.company_id', $companyId)
                                            ->where(function ($query) {
                                                $query->where('master_medical_checkups.uric_acid_level', '<', 0)
                                                    ->orWhere('master_medical_checkups.uric_acid_level', '>=', 7);
                                            })
                                            ->get()
                                            ->groupBy(function($date) {
                                                return Carbon::parse($date->created_at)->format('m');
                                            });

        $normalData = MasterMedicalCheckup::join('employees', 'employees.id', '=', 'master_medical_checkups.employee_id')
                                            ->where('employees.company_id', $companyId)
                                            ->where(function ($query) {
                                                $query->where('master_medical_checkups.uric_acid_level', '>', 0)
                                                    ->orWhere('master_medical_checkups.uric_acid_level', '<', 7);
                                            })
                                            ->get()
                                            ->groupBy(function($date) {
                                                return Carbon::parse($date->created_at)->format('m');
                                            });

        $periodCounter = [];
        $normalDataPeriodCounter = [];

        foreach ($uricAcidData as $key => $value) {
            $periodCounter[(int)$key] = count($value);
        }

        foreach ($normalData as $key => $value) {
            $normalDataPeriodCounter[(int)$key] = count($value);
        }

        $finalData = [];

        for($i = 1; $i <= 12; $i++){
            if(!empty($periodCounter[$i]) && !empty($normalDataPeriodCounter[$i])){
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => $normalDataPeriodCounter[$i],
                    'uricAcid' => $periodCounter[$i]
                ];
            } else if (empty($periodCounter[$i]) && !empty($normalDataPeriodCounter[$i])) {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => $normalDataPeriodCounter[$i],
                    'uricAcid' => 0
                ];
            } else if (!empty($periodCounter[$i]) && empty($normalDataPeriodCounter[$i])) {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => 0,
                    'uricAcid' => $periodCounter[$i]
                ];
            } else {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'normal' => 0,
                    'uricAcid' => 0
                ];
            }
        }

        return $finalData;
    }

    private function getHypertensionTotal($companyId): array
    {
        $stageOneHypertensionData = MasterMedicalCheckup::join('employees', 'employees.id', '=', 'master_medical_checkups.employee_id')
                                                        ->where('employees.company_id', $companyId)
                                                        ->where(function ($query) {
                                                            $query->where('master_medical_checkups.sistole', '<', 140)
                                                                ->where('master_medical_checkups.diastole', '<', 100);
                                                        })
                                                        ->get()
                                                        ->groupBy(function($date) {
                                                            return Carbon::parse($date->created_at)->format('m');
                                                        });

        $stageTwoHypertensionData = MasterMedicalCheckup::join('employees', 'employees.id', '=', 'master_medical_checkups.employee_id')
                                                        ->where('employees.company_id', $companyId)
                                                        ->where(function ($query) {
                                                            $query->where('master_medical_checkups.sistole', '>', 140)
                                                                ->where('master_medical_checkups.diastole', '>=', 100);
                                                        })
                                                        ->get()
                                                        ->groupBy(function($date) {
                                                            return Carbon::parse($date->created_at)->format('m');
                                                        });

        $stageOnePeriodCounter = [];

        foreach ($stageOneHypertensionData as $key => $value) {
            $stageOnePeriodCounter[(int)$key] = count($value);
        }

        $stageTwoPeriodCounter = [];

        foreach ($stageTwoHypertensionData as $key => $value) {
            $stageTwoPeriodCounter[(int)$key] = count($value);
        }

        $finalData = [];

        for ($i = 1; $i <= 12; $i++) {
            if (!empty($stageOnePeriodCounter[$i]) && !empty($stageTwoPeriodCounter[$i])) {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'stageOne' => $stageOnePeriodCounter[$i],
                    'stageTwo' => $stageTwoPeriodCounter[$i]
                ];
            } else if (!empty($stageOnePeriodCounter[$i]) && empty($stageTwoPeriodCounter[$i])) {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'stageOne' => $stageOnePeriodCounter[$i],
                    'stageTwo' => 0
                ];
            } else if (empty($stageOnePeriodCounter[$i]) && !empty($stageTwoPeriodCounter[$i])) {
                $finalData[$i] = (object)[
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'stageOne' => 0,
                    'stageTwo' => $stageTwoPeriodCounter[$i]
                ];
            } else {
                $finalData[$i] = (object) [
                    'period' => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'stageOne' => 0,
                    'stageTwo' => 0,
                ];
            }
        }

        return $finalData;
    }

    private function getBodyMaxIndexTotal($companyId): array
    {
        $masterMedicalCheckupData = MasterMedicalCheckup::join('employees', 'employees.id', '=', 'master_medical_checkups.employee_id')
                                                        ->where('employees.company_id', $companyId)
                                                        ->get()
                                                        ->groupBy(function($date) {
                                                            return Carbon::parse($date->created_at)->format('m');
                                                        });

        $periodCounter = [];
        $obesse = 0;
        $normal = 0;
        $underweight = 0;
        $overweight = 0;

        foreach ($masterMedicalCheckupData as $key => $value) {
            foreach($value as $val) {
                if ($val->weight > 0 && $val->height > 0) {
                    $bmi = round($val->weight/(pow(($val->height/100),2)), 2);

                    if ($bmi > 0 && $bmi < 18.5) {
                        $underweight +=1;
                    } else if ($bmi >= 18.5 && $bmi < 23) {
                        $normal +=1;
                    } else if ($bmi >= 23 && $bmi < 25) {
                        $overweight += 1;
                    } else if ($bmi >= 25) {
                        $obesse += 1;
                    }
                }
            }

            $periodCounter[(int)$key] = (object) [
                'underweight'  => $underweight,
                'normal'    => $normal,
                'overweight' => $overweight,
                'obesse'  => $obesse,
            ];
        }

        $finalData = [];

        for($i = 1; $i <= 12; $i++){
            if(!empty($periodCounter[$i])){
                $periodCounter[$i]->period = Carbon::create()->month($i)->startOfMonth()->format('M');
                $finalData[$i] = $periodCounter[$i];
            }else{
                $finalData[$i] = (object) [
                    'period'    => Carbon::create()->month($i)->startOfMonth()->format('M'),
                    'underweight'  => 0,
                    'normal'    => 0,
                    'overweight' => 0,
                    'obesse'  => 0,
                ];
            }
        }

        return $finalData;
    }

    private function generatePeriodData($data): array
    {
        $periodCounter = [];

        foreach ($data as $key => $value) {
            $periodCounter[(int)$key] = count($value);
        }

        $finalData = [];

        for($i = 1; $i <= 12; $i++){
            if(!empty($periodCounter[$i])){
                $finalData[$i] = $periodCounter[$i];
            }else{
                $finalData[$i] = 0;
            }
        }

        return $finalData;
    }


    public function importData($request): object
    {
        // Read data from file
        (new FastExcel)->import($request->file('file'), function ($line) {
            switch($line) {
                case empty($line['Nama Lengkap']):
                case empty($line['Nama Perusahaan']):
                case empty($line['Tanggal Lahir']):
                case empty($line['Jenis Kelamin']):
                case empty($line['Berat Badan (kg)']):
                case empty($line['Tinggi Badan (cm)']):
                    $medicalCheckupData = $this->getMedicalCheckupDataFromFile($line, 'declined');

                    $this->declinedMedicalCheckupsData[] = $medicalCheckupData;

                    break;
                default:
                    $medicalCheckupData = $this->getMedicalCheckupDataFromFile($line, 'imported');

                    $this->importedMedicalCheckupsData[] = $medicalCheckupData;

                    break;
            }
        });

        MasterMedicalCheckup::insert($this->importedMedicalCheckupsData);

        return (object)[
            'declined'    => $this->declinedMedicalCheckupsData
        ];
    }

    /**
     * Define data based on imported file
     *
     * @param $line
     * @param $type
     * @return array
     */
    private function getMedicalCheckupDataFromFile($line, $type): array
    {
        $collection = [];
        $collection['nik'] = $line['NIK'] ?? "-";
        $collection['height'] = $line['Tinggi Badan (cm)'];
        $collection['weight'] = $line['Berat Badan (kg)'];
        $collection['sistole'] = $line['Tekanan Darah Atas'] ?? 0;
        $collection['diastole'] = $line['Tekanan Darah Bawah'] ?? 0;
        $collection['blood_sugar_level'] = $line['Gula Darah'] ?? 0;
        $collection['cholesterol_level'] = $line['Kolesterol'] ?? 0;
        $collection['uric_acid_level'] = $line['Asam Urat'] ?? 0;

        if ($type == "imported") {
            $employee = DB::table('employees')
                ->select('id')
                ->where('nik', '=', $line['NIK'])
                ->first();

            if (isset($employee)) {
                $collection['employee_id'] = $employee->id;
                $collection['employee_registered'] = true;
            } else {
                $collection['employee_id'] = 100000;
                $collection['employee_registered'] = false;
            }

            return $collection;
        } else {
            return $collection;
        }
    }
}

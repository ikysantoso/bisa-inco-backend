<?php

namespace App\Repository\MedicalCheckup\V1;

interface MedicalCheckupRepositoryInterface
{
    public function getAll();
    public function getById($id);
    public function create($request);
    public function update($request, $id);
    public function delete($id);
    public function getAllMasterMedicalCheckup();
    public function getAllMasterMedicalCheckupByCompanyId($companyId);
    public function deleteMasterMedicalCheckup($id);
    public function getPercentageMedicalCheckup($companyId, $specific_diagnose, $diagnose);
    public function getMedicalCheckupByStatus($status, $companyId);
    public function getDiagnoseTotalMedicalCheckup($companyId);
    public function importData($request);
}

<?php

namespace App\Repository\NakesSpecial\V1;

use App\Models\MasterMedicalCheckup;
use App\Models\NakesSpecials;
use App\Models\Employee;
use Illuminate\Support\Facades\Crypt;

class NakesSpecialRepository implements NakesSpecialRepositoryInterface
{
    public function store($request)
    {
        $exist_nakes = NakesSpecials::where('nursing_number', '=', $request->nursing_number)
                                    ->where('company_id', '=', Crypt::decryptString($request->company_id))
                                    ->first();

        if (isset($exist_nakes)) {
            return null;
        } else {
            $input['company_id'] = Crypt::decryptString($request->company_id);
            $input['nursing_number'] = $request->nursing_number;
            $input['fullname'] = $request->fullname;
            $input['nik'] = $request->nik;
            $input['phone'] = $request->phone;
            $input['instance_name'] = $request->instance_name;
            $input['instance_address'] = $request->instance_address;

            return NakesSpecials::create($input);
        }
    }

    public function getByNursingNumber($company_id, $nursing_number)
    {
        $exist_nakes = NakesSpecials::where('nursing_number', '=', $nursing_number)
            ->where('company_id', '=', $company_id)
            ->first();

        if (isset($exist_nakes)) {
            return $exist_nakes;
        } else {
            $url = env('BISA_NAKES_BASE_API') . '/team/' . $nursing_number;
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $url);

            return json_decode($response->getBody(), true);
        }
    }

    public function getByMasterMedicalCheckupId($master_medical_checkup_id)
    {
        $master_medical_checkup = MasterMedicalCheckup::find($master_medical_checkup_id);

        $exist_nakes = NakesSpecials::where('nursing_number', '=', $master_medical_checkup->nursing_number)
            ->where('company_id', '=', $master_medical_checkup->employee->company_id)
            ->first();

        if (isset($exist_nakes)) {
            return $exist_nakes;
        } else {
            $url = env('BISA_NAKES_BASE_API') . '/team/' . $master_medical_checkup->nursing_number;
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $url);

            return json_decode($response->getBody(), true);
        }
    }

    public function getAllByCompany($company_id)
    {
        return NakesSpecials::where('company_id', '=', $company_id)->get();
    }

    public function deleteNurse($id)
    {
        $nakesSpecials = NakesSpecials::find($id);

        $medical_checkups_by_selected_nakes = MasterMedicalCheckup::where('nursing_number', '=', $nakesSpecials->nursing_number)
                                                                    ->whereHas('employee', function ($query) use ($nakesSpecials) {
                                                                        $query->where('company_id', '=',  $nakesSpecials->company_id);
                                                                    })
                                                                    ->where('inputted_by_special_nakes', '=', true)
                                                                    ->get();

        foreach ($medical_checkups_by_selected_nakes as $keyMedicalCheckup => $medical_checkup) {
            $medical_checkup->inputted_by_special_nakes = false;
            $medical_checkup->save();
        }

        $nakesSpecials->delete();

        return $nakesSpecials;
    }
}

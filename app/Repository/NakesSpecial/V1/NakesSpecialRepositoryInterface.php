<?php

namespace App\Repository\NakesSpecial\V1;

interface NakesSpecialRepositoryInterface
{
    public function store($request);
    public function getByNursingNumber($company_id, $nursing_number);
    public function getByMasterMedicalCheckupId($master_medical_checkup_id);
    public function getAllByCompany($company_id);
    public function deleteNurse($id);
}

<?php

namespace App\Repository\Company\V1;

interface CompanyRepositoryInterface
{
    public function getAll();
    public function getById($id);
    public function create($request);
    public function update($request, $id);
    public function delete($id);
    public function getFitnessEmployeeByCompany($companyId);
}

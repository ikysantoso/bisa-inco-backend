<?php

namespace App\Repository\Company\V1;

use App\Http\Resources\V1\CompanyResource;
use App\Models\Company;
use App\Models\Fitness;
use GuzzleHttp\Client;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Str;

class CompanyRepository implements CompanyRepositoryInterface
{
    public function getAll(): AnonymousResourceCollection
    {
        return CompanyResource::collection(Company::all());
    }

    public function getById($id): ?CompanyResource
    {
        $company = Company::find($id);

        if ($company) {
            return new CompanyResource($company);
        } else {
            return null;
        }
    }

    public function create($request): ?CompanyResource
    {
        $exist_company = Company::where('company_name', '=', $request->companyName)
                                ->first();

        if (isset($exist_company)) {
            return null;
        } else {
            // Company id generator
            $generated_company_id = mt_rand(10, 99) . '.' . strtoupper(Str::random(3)) . '.' . mt_rand(10000, 99999);

            $input['company_id'] = $generated_company_id;
            $input['company_name'] = $request->companyName;

            return new CompanyResource(Company::create($input));
        }
    }

    public function update($request, $id): ?CompanyResource
    {
        $company = Company::find($id);

        if ($company) {
            $company->company_name = $request->companyName;

            // Inputted data specific with chosen company
            if ($company->isClean()) {
                return $this->apiController->errorResponse('Tidak ada perubahan data yang anda masukan', null);
            }

            $company->save();

            return new CompanyResource($company);
        } else {
            return null;
        }
    }

    public function delete($id)
    {
        $company = Company::find($id);

        if ($company) {
            $company->delete();

            return new CompanyResource($company);
        } else {
            return null;
        }
    }

    public function getFitnessEmployeeByCompany($companyId)
    {
        $company = Company::find($companyId);

        if ($company) {
            foreach($company->employees as $key => $value) {
                // Setup for BISA Sehat communication
                $url = env('BISA_SEHAT_BASE_API') . '/inco/getLatestFitness/' . $value['nik'];
                $client = new Client();
                $response = $client->request('GET', $url);

                $fitnessData = json_decode($response->getBody(), true);

                // If user has fitness data in BISA Sehat, data will be inserted to BISA Inco fitness database
                if (isset($fitnessData['data']) && count($fitnessData['data']['fitnesses']) > 0) {
                    $lastFitness = $fitnessData['data']['fitnesses'][0];

                    $input_fitness['employee_id'] = $value['id'];
                    $input_fitness['height'] = max($lastFitness['height'], 0);
                    $input_fitness['weight'] = max($lastFitness['weight'], 0);
                    $input_fitness['mileage'] = max($lastFitness['mileage'], 0);
                } else {
                    $input_fitness['employee_id'] = $value['id'];
                    $input_fitness['height'] = 0;
                    $input_fitness['weight'] = 0;
                    $input_fitness['mileage'] = 0;
                }

                Fitness::create($input_fitness);
            }
        } else {
            return null;
        }
    }
}

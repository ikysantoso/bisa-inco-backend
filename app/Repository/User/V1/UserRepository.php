<?php

namespace App\Repository\User\V1;

use App\Http\Resources\V1\CompanyResource;
use App\Http\Resources\V1\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;

class UserRepository implements UserRepositoryInterface
{
    public function getAll()
    {
        return UserResource::collection(User::all());
    }

    public function getById($id)
    {
        return new UserResource(User::with('company')->find(Crypt::decryptString($id)));
    }

    public function create($request)
    {
        return new UserResource(User::create($request->all()));
    }

    public function update($request, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        $user = User::find(Crypt::decryptString($id));

        if ($user) {
            $user->delete();
            return $user;
        } else {
            return null;
        }
    }

    public function getCompanyByUser($id)
    {
        $user = User::find(Crypt::decryptString($id));

        if ($user) {
            return new CompanyResource($user->company()->first());
        } else {
            return null;
        }
    }

    public function getUserByCompanyId($companyId)
    {
        $users = User::where('company_id', $companyId)->get();

        return UserResource::collection($users);
    }

    public function getUserDeleteByCompanyId($companyId)
    {
        $users = User::where('company_id', $companyId)->first();
        $users->delete();

        return $users;
    }
}

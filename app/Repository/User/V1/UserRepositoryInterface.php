<?php

namespace App\Repository\User\V1;

interface UserRepositoryInterface
{
    public function getAll();
    public function getById($id);
    public function create($request);
    public function update($request, $id);
    public function delete($id);
    public function getCompanyByUser($id);
    public function getUserByCompanyId($companyId);
    public function getUserDeleteByCompanyId($companyId);
}

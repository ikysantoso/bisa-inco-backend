<?php

namespace App\Repository\ResetPasswordRequest\V1;

interface ResetPasswordRequestRepositoryInterface
{
    public function requestResetPassword($email, $company_id);
    public function validateUniqueAccess($unique_access);
    public function doResetPassword($unique_access, $new_password);
}

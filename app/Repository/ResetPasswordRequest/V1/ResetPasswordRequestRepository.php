<?php

namespace App\Repository\ResetPasswordRequest\V1;

use App\Models\ResetPasswordRequest;
use App\Models\User;
use App\Traits\MailService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use SendinBlue\Client\ApiException;

class ResetPasswordRequestRepository implements ResetPasswordRequestRepositoryInterface
{
    use MailService;

    /**
     * @throws ApiException
     */
    public function requestResetPassword($email, $company_id)
    {
        $user = User::where('email', '=', $email)
                    ->where('company_id', '=', Crypt::decryptString($company_id))
                    ->first();

        if (isset($user)) {
            $exist_request = ResetPasswordRequest::where('email', '=', $email)
                                                ->where('company_id', '=', Crypt::decryptString($company_id))
                                                ->where('used_at', '=', null)
                                                ->orderBy('created_at', 'desc')
                                                ->first();

            if (isset($exist_request)) {
                $exist_link_expires_time = Carbon::createFromFormat('Y-m-d H:s:i', $exist_request->link_expires_time);
                $diff = $exist_link_expires_time->diffInHours(Carbon::now());

                if ($diff > 1) {
                    $input['company_id'] = Crypt::decryptString($company_id);
                    $input['email'] = $email;
                    $input['unique_access'] = rand(11111, 99999);
                    $input['link_expires_time'] = Carbon::now()->addHour();

                    $response = ResetPasswordRequest::create($input);

                    $this->resetPasswordMail($user->name, $email, Crypt::encryptString($response->unique_access));

                    return 'success';
                } else {
                    return 'already exist, wait next hour';
                }
            } else {
                $input['company_id'] = Crypt::decryptString($company_id);
                $input['email'] = $email;
                $input['unique_access'] = rand(11111, 99999);
                $input['link_expires_time'] = Carbon::now()->addHour();

                $response = ResetPasswordRequest::create($input);

                $this->resetPasswordMail($user->name, $email, Crypt::encryptString($response->unique_access));

                return 'success';
            }
        } else {
            return 'user not exist';
        }
    }

    public function validateUniqueAccess($unique_access)
    {
        if (!isset($unique_access) || $unique_access == "") {
            return 'unique access cannot be empty while request';
        }

        $exist_request = ResetPasswordRequest::where('unique_access', '=', Crypt::decryptString($unique_access))
                                            ->where('used_at', '=', null)
                                            ->orderBy('created_at', 'desc')
                                            ->first();

        if (isset($exist_request)) {
            $user = User::where('email', '=', $exist_request->email)
                        ->where('company_id', '=', $exist_request->company_id)
                        ->first();

            if (isset($user)) {
                $exist_link_expires_time = Carbon::createFromFormat('Y-m-d H:s:i', $exist_request->link_expires_time);
                $diff = $exist_link_expires_time->diffInHours(Carbon::now());

                if ($diff > 1) {
                    return 'expired';
                } else {
                    return 'success';
                }
            } else {
                return 'user not found';
            }
        } else {
            return 'access not exist';
        }
    }

    public function doResetPassword($unique_access, $new_password)
    {
        if (!isset($unique_access) || $unique_access == "") {
            return 'unique access cannot be empty while request';
        }

        if (!isset($new_password) || $new_password == "") {
            return 'new password cannot be empty while request';
        }

        $exist_request = ResetPasswordRequest::where('unique_access', '=', Crypt::decryptString($unique_access))
                                            ->where('used_at', '=', null)
                                            ->orderBy('created_at', 'desc')
                                            ->first();

        if (isset($exist_request)) {
            $user = User::where('email', '=', $exist_request->email)
                ->where('company_id', '=', $exist_request->company_id)
                ->first();

            if (isset($user)) {
                $exist_link_expires_time = Carbon::createFromFormat('Y-m-d H:s:i', $exist_request->link_expires_time);
                $diff = $exist_link_expires_time->diffInHours(Carbon::now());

                if ($diff > 1) {
                    return 'expired';
                } else {
                    $user->password = Hash::make($new_password);
                    $user->save();

                    $exist_request->used_at = Carbon::now();
                    $exist_request->save();

                    return 'success';
                }
            } else {
                return 'user not found';
            }
        } else {
            return 'access not exist';
        }
    }
}

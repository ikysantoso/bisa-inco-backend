<?php

namespace App\Repository\Employee\V1;

use App\Http\Resources\V1\DeclinedVerificationRequestResource;
use App\Http\Resources\V1\EmployeeDeleteRequestResource;
use App\Http\Resources\V1\EmployeeResource;
use App\Http\Resources\V1\EmployeeVerificationRequestResource;
use App\Models\Company;
use App\Models\DeclinedVerificationRequest;
use App\Models\Employee;
use App\Models\EmployeeDeleteRequest;
use App\Models\EmployeeVerificationRequest;
use App\Models\Fitness;
use App\Models\MasterMedicalCheckup;
use App\Models\MedicalCheckup;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Crypt;

class EmployeeRepository implements EmployeeRepositoryInterface
{
    public function requestVerification($request)
    {
        $company = Company::where('company_id', '=', $request->companyId)->first();

        if (isset($company)) {
            $input['company_id'] = $company->id;
            $input['request_id'] = 'ReqEmp'.rand(1234,9999);
            $input['nik'] = $request->nik;
            $input['fullname'] = $request->fullname;
            $input['birthdate'] = Carbon::createFromFormat('m/d/Y', $request->birthdate);
            $input['division'] = $request->division;

            return new EmployeeResource(EmployeeVerificationRequest::create($input));
        } else {
            return null;
        }
    }

    public function requestDelete($request)
    {
        $company = Company::where('company_id', $request->companyId)->first();

        $input['company_id'] = $company->id;
        $input['request_id'] = 'ReqDeleteEmp'.rand(1234,9999);
        $input['nik'] = $request->nik;
        $input['fullname'] = $request->fullname;
        $input['birthdate'] = Carbon::createFromFormat('m/d/Y', $request->birthdate);
        $input['division'] = $request->division;

        return new EmployeeResource(EmployeeDeleteRequest::create($input));
    }

    public function approvalRequest($request)
    {
        $verification_request = EmployeeVerificationRequest::where('request_id', $request->requestId)->first();

        if ($request->approvalStatus == 'approve') {
            $input['company_id'] = $verification_request->company_id;
            $input['employee_id'] = 'Emp'.rand(1234, 9999);
            $input['nik'] = $verification_request->nik;
            $input['fullname'] = $verification_request->fullname;
            $input['birthdate'] = Carbon::createFromFormat('Y-m-d', $verification_request->birthdate);
            $input['division'] = $verification_request->division;
            $input['gender'] = $this->getGenderByNIK($verification_request->nik);
            $employee =  Employee::create($input);

            $user_medical_checkups = $this->getMedicalCheckupFromAplikasiBISAByNik($verification_request->nik);

            $input_master_medical_checkup['employee_id'] = $employee->id;
            $input_master_medical_checkup['height'] = $user_medical_checkups['data']['medical_records'][0]['height'] ?? 0;
            $input_master_medical_checkup['weight'] = $user_medical_checkups['data']['medical_records'][0]['weight'] ?? 0;
            $input_master_medical_checkup['sistole'] = $user_medical_checkups['data']['medical_records'][0]['sistole'] ?? 0;
            $input_master_medical_checkup['diastole'] = $user_medical_checkups['data']['medical_records'][0]['diastole'] ?? 0;
            $input_master_medical_checkup['blood_sugar_level'] = $user_medical_checkups['data']['medical_records'][0]['blood_sugar_level'] ?? 0;
            $input_master_medical_checkup['cholesterol_level'] = $user_medical_checkups['data']['medical_records'][0]['cholesterol_level'] ?? 0;
            $input_master_medical_checkup['uric_acid_level'] = $user_medical_checkups['data']['medical_records'][0]['uric_acid_level'] ?? 0;
            $input_master_medical_checkup['checkup_date'] = Carbon::now();
            MasterMedicalCheckup::create($input_master_medical_checkup);

            foreach($user_medical_checkups['data']['medical_records'] as $medicalRecords => $record) {
                $input_medical_checkup['employee_id'] = $employee->id;
                $input_medical_checkup['height'] = $record['height'] ?? 0;
                $input_medical_checkup['weight'] = $record['weight'] ?? 0;
                $input_medical_checkup['sistole'] = $record['sistole'] ?? 0;
                $input_medical_checkup['diastole'] = $record['diastole'] ?? 0;
                $input_medical_checkup['blood_sugar_level'] = $record['blood_sugar_level'] ?? 0;
                $input_medical_checkup['cholesterol_level'] = $record['cholesterol_level'] ?? 0;
                $input_medical_checkup['uric_acid_level'] = $record['uric_acid_level'] ?? 0;
                $input_medical_checkup['checkup_date'] = Carbon::now();

                MedicalCheckup::create($input_medical_checkup);
            }

            $user_fitness = $this->getEmployeeLatestFitnessByNIK($verification_request->nik);

            if (count($user_fitness) > 0) {
                $input_fitness['employee_id'] = $employee->id;
                $input_fitness['height'] = $user_fitness[0]['height'];
                $input_fitness['weight'] = $user_fitness[0]['weight'];
                $input_fitness['mileage'] = $user_fitness[0]['mileage'];
                Fitness::create($input_fitness);
            }

            $verification_request->delete();

            return $user_medical_checkups;
        } else {
            $input['request_id'] = $verification_request->request_id;
            $input['nik'] = $verification_request->nik;
            $input['fullname'] = $verification_request->fullname;
            $input['birthdate'] = $verification_request->birthdate;
            $input['division'] = $verification_request->division;
            $declined_verification = DeclinedVerificationRequest::create($input);

            $verification_request->delete();

            return new DeclinedVerificationRequestResource($declined_verification);
        }
    }

    public function getAllVerificationRequestByCompanyId($id): AnonymousResourceCollection
    {
        $company = Company::where('company_id', $id)->first();

        return EmployeeVerificationRequestResource::collection(EmployeeVerificationRequest::where('company_id', $company->id)->get());
    }

    public function getAllDeleteRequestByCompanyId($id): AnonymousResourceCollection
    {
        $company = Company::where('company_id', $id)->first();

        return EmployeeDeleteRequestResource::collection(EmployeeDeleteRequest::where('company_id', $company->id)->get());
    }

    public function getAllDeclinedVerification(): AnonymousResourceCollection
    {
        return DeclinedVerificationRequestResource::collection(DeclinedVerificationRequest::all());
    }

    public function getAll($request): AnonymousResourceCollection
    {
        if (isset($request->companyId)) {
            return EmployeeResource::collection(Employee::where('company_id', Crypt::decryptString($request->companyId))->get());
        } else {
            return EmployeeResource::collection(Employee::all());
        }
    }

    public function getById($id): EmployeeResource
    {
        return new EmployeeResource(Employee::find(Crypt::decryptString($id)));
    }

    public function getAllEmployeeHealthStatus(): object
    {
        $cholesterol = MasterMedicalCheckup::query()
            ->where('cholesterol_level', '>', 1)
            ->count();

        $bloodSugar = MasterMedicalCheckup::query()
            ->where('blood_sugar_level', '>', 1)
            ->count();

        $notRecorded = MasterMedicalCheckup::query()
            ->where('sistole', 0)
            ->where('diastole', 0)
            ->where('blood_sugar_level', 0)
            ->where('cholesterol_level', 0)
            ->where('uric_acid_level', 0.0)
            ->count();

        return (object) [
          'cholesterol' => $cholesterol,
          'bloodSugar' => $bloodSugar,
          'notRecorded' => $notRecorded,
        ];
    }

    public function getEmployeeRequestByNIK($nik)
    {
        $employee = EmployeeVerificationRequest::where('nik', $nik)->first();

        if ($employee) {
            return new EmployeeVerificationRequestResource($employee);
        } else {
            return null;
        }
    }

    public function getEmployeeRequestDeleteByNIK($nik)
    {
        $employee = EmployeeDeleteRequest::where('nik', $nik)->first();

        if ($employee) {
            return new EmployeeDeleteRequestResource($employee);
        } else {
            return null;
        }
    }

    public function getByNIK($nik)
    {
        $employee = Employee::where('nik', $nik)->first();

        if ($employee) {
            return new EmployeeResource($employee);
        } else {
            return null;
        }
    }

    private function getMedicalCheckupFromAplikasiBISAByNik($nik)
    {
        $url = env('BISA_SEHAT_BASE_API') . '/inco/getLatestMedicalRecordPerUser/' . $nik;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);

        return json_decode($response->getBody(), true);
    }

    public function delete($id)
    {
        $employee = Employee::find(Crypt::decryptString($id));

        $employee->delete();

        return $employee;
    }

    public function deleteEmployeeVerificationRequest($id)
    {
        $employee = EmployeeVerificationRequest::find(Crypt::decryptString($id));

        $employee->delete();

        return $employee;
    }

    public function deleteEmployeeRequest($id)
    {
        $employeeDeleteRequest = EmployeeDeleteRequest::find(Crypt::decryptString($id));
        $employeeDeleteRequest->delete();

        $employee = Employee::where('nik', $employeeDeleteRequest->nik)->where('company_id', $employeeDeleteRequest->company_id)->first();
        $employee->delete();

        return $employeeDeleteRequest;
    }

    public function getEmployeeRequestByCompanyId($companyId)
    {
        $employee = EmployeeVerificationRequest::where('company_id', $companyId)->get();

        if ($employee) {
            return EmployeeVerificationRequestResource::collection($employee);
        } else {
            return null;
        }
    }

    public function getEmployeeDeleteByCompanyId($companyId)
    {
        $employee = EmployeeDeleteRequest::where('company_id', $companyId)->get();

        if ($employee) {
            return EmployeeDeleteRequestResource::collection($employee);
        } else {
            return null;
        }
    }

    public function getAllEmployeeByCompanyId($companyId)
    {
        $employee = Employee::where('company_id', $companyId)->get();

        if ($employee) {
            return EmployeeResource::collection($employee);
        } else {
            return null;
        }
    }

    private function getEmployeeLatestFitnessByNIK($nik)
    {
        $url = env('BISA_SEHAT_BASE_API') . '/inco/getLatestFitness/' . $nik;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);

        $user_fitness = json_decode($response->getBody(), true);

        return $user_fitness['data']['fitnesses'];
    }

    private function getGenderByNIK($nik)
    {
        $url = env('BISA_SEHAT_BASE_API') . '/inco/getGender/' . $nik;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);

        $user_gender = json_decode($response->getBody(), true);

        return $user_gender['data'];
    }
}

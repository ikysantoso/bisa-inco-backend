<?php

namespace App\Repository\Employee\V1;

interface EmployeeRepositoryInterface
{
    public function requestVerification($request);
    public function requestDelete($request);
    public function approvalRequest($request);
    public function getAllVerificationRequestByCompanyId($id);
    public function getAllDeleteRequestByCompanyId($id);
    public function getAllDeclinedVerification();
    public function getAll($request);
    public function getById($id);
    public function getAllEmployeeHealthStatus();
    public function getEmployeeRequestByNIK($nik);
    public function getEmployeeRequestDeleteByNIK($nik);
    public function getByNIK($nik);
    public function delete($id);
    public function deleteEmployeeVerificationRequest($id);
    public function deleteEmployeeRequest($id);
    public function getEmployeeRequestByCompanyId($companyId);
    public function getEmployeeDeleteByCompanyId($companyId);
    public function getAllEmployeeByCompanyId($companyId);
}

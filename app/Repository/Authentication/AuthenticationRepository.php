<?php

namespace App\Repository\Authentication;

use App\Http\Resources\V1\UserResource;
use App\Models\AuthSession;
use App\Models\Company;
use App\Models\User;
use App\Traits\MailService;
use Illuminate\Support\Facades\Hash;
use SendinBlue\Client\ApiException;

class AuthenticationRepository implements AuthenticationRepositoryInterface
{
    use MailService;

    /**
     * @throws ApiException
     */
    public function register($request)
    {
        $company = Company::find($request->companyId);

        if ($company) {
            $input = array();
            $input['company_id'] = $request->companyId;
            $input['name'] = $request->name;
            $input['email'] = $request->email;
            $input['role'] = $request->role;
            $input['access_status'] = 'all';

            // Generated 4 digits random number
            $password = random_int(1111, 5555);
            $input['password'] = Hash::make($password);

            $existUser = User::where('email', '=', $request->email)
                ->where('company_id', '=', $request->companyId)
                ->first();

            // Validate if user / account exist for chosen company
            if ($existUser) {
                return 'exist';
            }

            $user = new UserResource(User::create($input));

            // Send email to registered account, password will be shown there
            $this->registrationNewUserForPartnerMail($user->name, $user->email, $password, $company->company_name);

            return $user;
        } else {
            return null;
        }
    }

    public function login($request)
    {
        $user = User::where('email', '=', $request->email)
                    ->where('company_id', '=', $request->companyId)
                    ->first();

        if ($user) {
            // Validate if hashed password match
            $checker = Hash::check($request->password, $user->password);

            if($checker) {
                $auth_session['user_id'] = $user->id;
                AuthSession::create($auth_session);

                return new UserResource($user);
            } else {
                return 'fail';
            }
        } else {
            return 'un-registered';
        }
    }

    public function logout($id): string
    {
        $auth_session = AuthSession::where('user_id', $id)
                                    ->first();

        if (isset($auth_session)) {
            return $auth_session->delete();
        } else {
            // Session login not found for the inputted user id
            return 'undefined';
        }
    }

    public function changePassword($request): string
    {
        $auth_session = AuthSession::where('user_id', $request->id)
                            ->first();

        if (isset($auth_session)) {
            $user = User::find($request->id);

            $checker = Hash::check($request->old_password, $user->password);

            if ($checker) {
                $hashed_new_password = Hash::make($request->new_password);

                $user->password = $hashed_new_password;
                $user->save();

                return 'successfully change password';
            } else {
                return 'wrong old password';
            }
        } else {
            return 'user undefined';
        }
    }
}



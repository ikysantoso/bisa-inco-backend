<?php

namespace App\Repository\Authentication;

interface AuthenticationRepositoryInterface
{
    public function register($request);
    public function login($request);
    public function logout($id);
    public function changePassword($request);
}

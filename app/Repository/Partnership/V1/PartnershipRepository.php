<?php

namespace App\Repository\Partnership\V1;

use App\Http\Resources\V1\PartnershipResource;
use App\Models\Company;

class PartnershipRepository implements PartnershipRepositoryInterface
{

    public function getAll()
    {
        return PartnershipResource::collection(Company::with('employees', 'users')->where('company_id', '!=', 'BISA')->get());
    }

    public function getById($id)
    {
        return new PartnershipResource(Company::find($id));
    }

}

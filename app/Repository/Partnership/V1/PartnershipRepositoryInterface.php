<?php

namespace App\Repository\Partnership\V1;

interface PartnershipRepositoryInterface
{
    public function getAll();
    public function getById($id);
}

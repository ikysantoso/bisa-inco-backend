<?php

namespace App\Http\Controllers\ResetPasswordRequest\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreResetPasswordRequestRequest;
use App\Http\Requests\UpdateResetPasswordRequestRequest;
use App\Models\ResetPasswordRequest;
use App\Repository\ResetPasswordRequest\V1\ResetPasswordRequestRepositoryInterface;
use Illuminate\Http\Request;

class ResetPasswordRequestController extends Controller
{
    protected $passwordRequestRepository;

    public function __construct(ResetPasswordRequestRepositoryInterface $passwordRequestRepository)
    {
        $this->passwordRequestRepository = $passwordRequestRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreResetPasswordRequestRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreResetPasswordRequestRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ResetPasswordRequest  $resetPasswordRequest
     * @return \Illuminate\Http\Response
     */
    public function show(ResetPasswordRequest $resetPasswordRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ResetPasswordRequest  $resetPasswordRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(ResetPasswordRequest $resetPasswordRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateResetPasswordRequestRequest  $request
     * @param  \App\Models\ResetPasswordRequest  $resetPasswordRequest
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateResetPasswordRequestRequest $request, ResetPasswordRequest $resetPasswordRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ResetPasswordRequest  $resetPasswordRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResetPasswordRequest $resetPasswordRequest)
    {
        //
    }

    public function requestResetPassword(Request $request)
    {
        $response = $this->passwordRequestRepository->requestResetPassword($request->email, $request->company_id);

        if ($response == "success") {
            return $this->successResponse('User', $response);
        } else {
            return $this->errorResponse('Wait for one hour to next request', $response);
        }
    }

    public function validateUniqueAccess(Request $request)
    {
        try {
            $response = $this->passwordRequestRepository->validateUniqueAccess($request->unique_access);

            if ($response == "access not exist") {
                return $this->errorResponse('Access not exist', null);
            } else {
                return $this->successResponse('Access approved', null);
            }
        } catch (\Exception $exception) {
            return $this->errorResponse('Access declined', null);
        }
    }

    public function doResetPassword(Request $request)
    {
        try {
            return $this->passwordRequestRepository->doResetPassword($request->unique_access, $request->new_password);
        } catch (\Exception $exception) {
            return 'unique access tidak sesuai';
        }
    }
}

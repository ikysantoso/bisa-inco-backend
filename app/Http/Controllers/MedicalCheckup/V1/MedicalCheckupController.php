<?php

namespace App\Http\Controllers\MedicalCheckup\V1;

use App\Http\Controllers\Controller;
use App\Repository\MedicalCheckup\V1\MedicalCheckupRepositoryInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;

class MedicalCheckupController extends Controller
{
    protected $medicalCheckupRepository;

    public function __construct(MedicalCheckupRepositoryInterface $medicalCheckupRepository)
    {
        $this->medicalCheckupRepository = $medicalCheckupRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $response = $this->medicalCheckupRepository->getAll();

        return $this->successResponse('Request berhasil', $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $response = $this->medicalCheckupRepository->create($request);

            return $this->successResponse('Request berhasil', $response);
        } catch (Exception $e) {
            return $this->errorResponse('ERROR', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        try {
            $response = $this->medicalCheckupRepository->getById(Crypt::decryptString($id));

            if ($response) {
                return $this->successResponse('Request berhasil', $response);
            } else {
                return $this->errorResponse('Data medical checkup tidak ditemukan', null);
            }
        } catch (Exception $exception) {
            return $this->errorResponse('Data medical checkup tidak ditemukan', null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Fetch all master medical checkup data
     *
     * @return JsonResponse
     */
    public function getAllMasterMedicalCheckup(): JsonResponse
    {
        $response = $this->medicalCheckupRepository->getAllMasterMedicalCheckup();

        return $this->successResponse('Request berhasil', $response);
    }

    /**
     * Fetch all master medical checkup data based on chosen company
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAllMasterMedicalCheckupByCompanyId(Request $request): JsonResponse
    {
        try {
            $response = $this->medicalCheckupRepository->getAllMasterMedicalCheckupByCompanyId(Crypt::decryptString($request->companyId));

            if (isset($response)) {
                return $this->successResponse('Request berhasil', $response);
            } else {
                return $this->errorResponse('Company tidak ditemukan', null);
            }
        } catch (Exception $exception) {
            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }

    /**
     * Delete master medical checkup data
     *
     * @param string $id
     * @return JsonResponse
     */
    public function deleteMasterMedicalCheckup(string $id): JsonResponse
    {
        try {
            $response = $this->medicalCheckupRepository->deleteMasterMedicalCheckup(Crypt::decryptString($id));

            return $this->successResponse('Request berhasil', $response);
        } catch (Exception $exception) {
            return $this->errorResponse('Data medical checkup tidak ditemukan', null);
        }
    }

    /**
     * Get all percentages medical checkup data based on chosen company
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getPercentageMedicalCheckup(Request $request): JsonResponse
    {
        try {
            $response = $this->medicalCheckupRepository->getPercentageMedicalCheckup(Crypt::decryptString($request->companyId), false, "");

            if (isset($response)) {
                return $this->successResponse('Berhasil mengambil data medical checkup', $response);
            } else {
                return $this->successResponse('Company tidak ditemukan', null);
            }
        } catch (Exception $e) {
            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }

    /**
     * Get medical checkup by status based on chosen company
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getMedicalCheckupByStatus(Request $request): JsonResponse
    {
        try {
            $response = $this->medicalCheckupRepository->getMedicalCheckupByStatus($request->status, Crypt::decryptString($request->companyId));

            return $this->successResponse('Berhasil mengambil data medical checkup', $response);
        } catch (Exception $e) {
            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }

    /**
     * Get diagnose total medical checkup data based on chosen company
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getDiagnoseTotalMedicalCheckup(Request $request): JsonResponse
    {
        try {
            $response = $this->medicalCheckupRepository->getDiagnoseTotalMedicalCheckup(Crypt::decryptString($request->companyId));

            if ($response) {
                return $this->successResponse('Berhasil mengambil data medical checkup', $response);
            } else {
                return $this->errorResponse('Company tidak ditemukan', null);
            }
        } catch (Exception $e) {
            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }

    /**
     * Get specific diagnose percentage based on chosen company
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getSpecificDiagnoseFromPercentage(Request $request): JsonResponse
    {
        try {
            $response = $this->medicalCheckupRepository->getPercentageMedicalCheckup(Crypt::decryptString($request->companyId), true, $request->diagnose);

            return $this->successResponse('Berhasil mengambil data medical checkup', $response);
        } catch (Exception $e) {
            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }

    /**
     * Import medical checkup data from excel/csv file
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function importData(Request $request): JsonResponse
    {
        try {
            $response = $this->medicalCheckupRepository->importData($request);

            return $this->successResponse('Import data berhasil', $response);
        } catch (Exception $exception) {
            return $this->serverErrorResponse();
        }
    }
}

<?php

namespace App\Http\Controllers\User\V1;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Employee;
use App\Models\MasterMedicalCheckup;
use App\Models\MedicalCheckup;
use App\Repository\User\V1\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{

    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $response = $this->userRepository->getAll();

        return $this->successResponse('Berhasil mengambil data user', $response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $response = $this->userRepository->create($request);

        return $this->successResponse('Berhasil membuat data user', $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $response = $this->userRepository->getById($id);

        return $this->successResponse('Berhasil mengambil data user', $response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $response = $this->userRepository->delete($id);

        return $this->successResponse('Berhasil menghapus data user', $response);
    }

    public function getCompanyByUser($id)
    {
        $response = $this->userRepository->getCompanyByUser($id);

        if ($response) {
            return $this->successResponse('Berhasil mengambil data company by user', $response);
        } else {
            return $this->errorResponse('User tidak ditemukan', null);
        }
    }

    public function getUserByCompanyId($companyId)
    {
        $response = $this->userRepository->getUserByCompanyId(Crypt::decryptString($companyId));

        return $this->successResponse('Berhasil mengambil data user by company', $response);
    }

    public function getUserDeleteByCompanyId($companyId)
    {
        $response = $this->userRepository->getUserDeleteByCompanyId(Crypt::decryptString($companyId));

        return $this->successResponse('Berhasil mengambil data user by company', $response);
    }

    public function importCSV(Request $request)
    {
        $file = $request->file('file');

        $company = Company::where('company_id', $request->companyId)->first();

        if ($company) {
            if ($file) {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $tempPath = $file->getRealPath();
                $fileSize = $file->getSize();
                
                $location = 'uploads' . '/' . $company->company_id;
                $file->move($location, $filename);
                $filepath = public_path($location . "/" . $filename);
                $file = fopen($filepath, "r");
                $importData_arr = array();
                
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
    
                    if ($i == 0) {
                        $i++;
                        continue;
                    }
    
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
    
                    $i++;
                }
    
                fclose($file);
    
                $j = 0;
                foreach ($importData_arr as $importData) {          
                    $j++;
                    
                    try {
                        $employee = Employee::where('fullname', $importData[0])
                                            ->where('division', $importData[5])
                                            ->where('company_id', $company->id)
                                            ->first();

                        if ($employee) {
                            DB::beginTransaction();

                            MedicalCheckup::create([
                                'employee_id'       => $employee->id,
                                'height'            => $importData[7],
                                'weight'            => $importData[8],
                                'cholesterol_level' => $importData[9],
                                'blood_sugar_level' => $importData[10],
                                'uric_acid_level'   => $importData[11],
                                'sistole'           => 0,
                                'diastole'          => 0
                            ]);

                            $master_medical_checkup = MasterMedicalCheckup::where('employee_id', $employee->id)->first();

                            if ($master_medical_checkup) {
                                $master_medical_checkup->height = $importData[7] == 0 ? $importData[7] : $master_medical_checkup->height;
                                $master_medical_checkup->weight = $importData[8] == 0 ? $importData[8] : $master_medical_checkup->weight;
                                $master_medical_checkup->cholesterol_level = $importData[9] == 0 ? $importData[9] : $master_medical_checkup->cholesterol_level;
                                $master_medical_checkup->blood_sugar_level = $importData[10] == 0 ? $importData[10] : $master_medical_checkup->blood_sugar_level;
                                $master_medical_checkup->uric_acid_level = $importData[11] == 0 ? $importData[11] : $master_medical_checkup->uric_acid_level;
                                $master_medical_checkup->save();
                            } else {
                                MasterMedicalCheckup::create([
                                    'employee_id'       => $employee->id,
                                    'height'            => $importData[7],
                                    'weight'            => $importData[8],
                                    'cholesterol_level' => $importData[9],
                                    'blood_sugar_level' => $importData[10],
                                    'uric_acid_level'   => $importData[11],
                                    'sistole'           => 0,
                                    'diastole'          => 0
                                ]);
                            }

                            DB::commit();
                        } else {
                            DB::beginTransaction();

                            Employee::create([
                                'company_id'    => $company->id,
                                'fullname'      => $importData[0],
                                'division'      => $importData[5],
                                'birthdate'     => Carbon::now(),
                                'employee_id'   => 'Emp'.rand(1234, 9999),
                                'nik'           => 0
                            ]);

                            DB::commit();
                        }
                    } catch (\Exception $e) {
                        Log::debug($e);
                        DB::rollBack();
                    }
                }
    
                return $this->successResponse('Data Berhasil di import', null);
            } else {
                return $this->errorResponse('File tidak ditemukan', null);
            }
        } else {
            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }

}

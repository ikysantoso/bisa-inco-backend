<?php

namespace App\Http\Controllers\Employee\V1;

use App\Http\Controllers\Controller;
use App\Repository\Employee\V1\EmployeeRepositoryInterface;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class EmployeeController extends Controller
{
    protected $employeeRepository;

    public function __construct(EmployeeRepositoryInterface $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function requestVerification(Request $request)
    {
        try {
            $response = $this->employeeRepository->requestVerification($request);

            if (isset($response)) {
                return $this->successResponse('Request berhasil', $response);
            } else {
                return $this->errorResponse('Company not found', null);
            }
        } catch (Exception $exception) {
            return $this->errorResponse('Company not found',null);
        }
    }

    public function requestDelete(Request $request)
    {
        $response = $this->employeeRepository->requestDelete($request);

        return $this->successResponse('Request berhasil', $response);
    }

    public function approvalRequest(Request $request)
    {
        try {
            $response = $this->employeeRepository->approvalRequest($request);

            return $this->successResponse('Approval berhasil', $response);
        } catch (Exception $e) {
            return $this->errorResponse('Server Error', $e->getMessage());
        }
    }

    public function getAllVerificationRequestByCompanyId($id)
    {
        $response = $this->employeeRepository->getAllVerificationRequestByCompanyId($id);

        return $this->successResponse('List diambil', $response);
    }

    public function getAllDeclinedVerification()
    {
        $response = $this->employeeRepository->getAllDeclinedVerification();

        return $this->successResponse('List diambil', $response);
    }

    public function index(Request $request)
    {
        $response = $this->employeeRepository->getAll($request);

        return $this->successResponse('Berhasil mengambil data employee', $response);
    }

    public function show($id)
    {
        $response = $this->employeeRepository->getById($id);

        return $this->successResponse('Berhasil mengambil data employee', $response);
    }

    public function getAllEmployeeHealthStatus()
    {
        $response = $this->employeeRepository->getAllEmployeeHealthStatus();

        return $this->successResponse('Berhasil mengambil data employee', $response);
    }

    public function getEmployeeRequestByNIK(Request $request)
    {
        $response = $this->employeeRepository->getEmployeeRequestByNIK($request->nik);

        if ($response) {
            return $this->successResponse('Berhasil mengambil data employee', $response);
        } else {
            return $this->errorResponse('Employee tidak ditemukan', null);
        }
    }

    public function getEmployeeRequestDeleteByNIK(Request $request)
    {
        $response = $this->employeeRepository->getEmployeeRequestDeleteByNIK($request->nik);

        if ($response) {
            return $this->successResponse('Berhasil mengambil data employee', $response);
        } else {
            return $this->errorResponse('Employee tidak ditemukan', null);
        }
    }

    public function getByNIK(Request $request)
    {
        $response = $this->employeeRepository->getByNIK($request->nik);

        if ($response) {
            return $this->successResponse('Berhasil mengambil data employee', $response);
        } else {
            return $this->errorResponse('Employee tidak ditemukan', null);
        }
    }

    public function destroy($id)
    {
        $response = $this->employeeRepository->delete($id);

        return $this->successResponse('Berhasil menghapus', $response);
    }

    public function deleteEmployeeVerificationRequest($id)
    {
        $response = $this->employeeRepository->deleteEmployeeVerificationRequest($id);

        return $this->successResponse('Berhasil menghapus', $response);
    }

    public function deleteEmployeeRequest($id)
    {
        $response = $this->employeeRepository->deleteEmployeeRequest($id);

        return $this->successResponse('Berhasil menghapus', $response);
    }

    public function getEmployeeRequestByCompanyId(Request $request)
    {
        $response = $this->employeeRepository->getEmployeeRequestByCompanyId(Crypt::decryptString($request->companyId));

        if ($response) {
            return $this->successResponse('Berhasil mengambil data employee', $response);
        } else {
            return $this->errorResponse('Employee tidak ditemukan', null);
        }
    }

    public function getEmployeeDeleteByCompanyId(Request $request)
    {
        $response = $this->employeeRepository->getEmployeeDeleteByCompanyId(Crypt::decryptString($request->companyId));

        if ($response) {
            return $this->successResponse('Berhasil mengambil data employee', $response);
        } else {
            return $this->errorResponse('Employee tidak ditemukan', null);
        }
    }

    public function getAllEmployeeByCompanyId(Request $request)
    {
        try {
            $response = $this->employeeRepository->getAllEmployeeByCompanyId(Crypt::decryptString($request->companyId));

            if ($response) {
                return $this->successResponse('Berhasil mengambil data employee', $response);
            } else {
                return $this->errorResponse('Employee tidak ditemukan', null);
            }
        } catch (Exception $e) {
            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }
}

<?php

namespace App\Http\Controllers\Fitness\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFitnessRequest;
use App\Http\Requests\UpdateFitnessRequest;
use App\Models\Employee;
use App\Models\Fitness;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FitnessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Fitness[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index()
    {
        return Fitness::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFitnessRequest $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $employee = Employee::where('nik', $request['nik'])->first();

            if(isset($employee)) {
                $fitness = Fitness::where('employee_id', $employee->id)->first();

                if ($fitness) {
                    $fitness->height = $request['height'];
                    $fitness->weight = $request['weight'];
                    $fitness->mileage = $request['mileage'];
                    $fitness->save();
                } else {
                    $request_input = $request->all();
                    $request_input['employee_id'] = $employee->id;

                    $fitness = Fitness::create($request_input);
                }

                return $this->successResponse('Request berhasil', $fitness);
            } else {
                return $this->errorResponse('Employee tidak di temukan', null);
            }
        } catch (\Exception $e) {
            return $this->errorResponse('ERROR', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Fitness  $fitness
     * @return \Illuminate\Http\Response
     */
    public function show(Fitness $fitness)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fitness  $fitness
     * @return \Illuminate\Http\Response
     */
    public function edit(Fitness $fitness)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFitnessRequest  $request
     * @param  \App\Models\Fitness  $fitness
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFitnessRequest $request, Fitness $fitness)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Fitness  $fitness
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fitness $fitness)
    {
        //
    }
}

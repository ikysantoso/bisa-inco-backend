<?php

namespace App\Http\Controllers\Company\V1;

use App\Http\Controllers\Controller;
use App\Repository\Company\V1\CompanyRepositoryInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    protected $companyRepository;

    public function __construct(CompanyRepositoryInterface $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $response = $this->companyRepository->getAll();

        return $this->successResponse('Request berhasil', $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $response = $this->companyRepository->create($request);

            if (isset($response)) {
                DB::commit();

                return $this->successResponse('Berhasil membuat company', $response);
            } else {
                DB::rollBack();

                return $this->errorResponse('Company sudah terdaftar', null);
            }
        } catch (Exception $exception) {
            DB::rollBack();

            return $this->serverErrorResponse();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        try {
            $response = $this->companyRepository->getById(Crypt::decryptString($id));

            if (isset($response)) {
                return $this->successResponse('Request berhasil', $response);
            } else {
                return $this->errorResponse('Company tidak ditemukan', null);
            }
        } catch (Exception $exception) {
            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  string  $id
     * @return JsonResponse
     */
    public function update(Request $request, string $id): JsonResponse
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $id
     * @return JsonResponse
     */
    public function destroy(string $id): JsonResponse
    {
        try {
            DB::beginTransaction();

            $response = $this->companyRepository->delete(Crypt::decryptString($id));

            if (isset($response)) {
                DB::commit();

                return $this->successResponse('Request berhasil', $response);
            } else {
                DB::rollBack();

                return $this->errorResponse('Company tidak ditemukan', null);
            }
        } catch (Exception $exception) {
            DB::rollBack();

            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }

    /**
     * Find all fitness data specified by company id
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getFitnessEmployeeByCompany(Request $request): JsonResponse
    {
        try {
            $company_id = Crypt::decryptString($request->companyId);

            $response = $this->companyRepository->getFitnessEmployeeByCompany($company_id);

            return $this->successResponse('Request berhasil', $response);
        } catch (Exception $exception) {
            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }
}

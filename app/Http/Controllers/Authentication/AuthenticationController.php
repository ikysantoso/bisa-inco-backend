<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use App\Repository\Authentication\AuthenticationRepositoryInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class AuthenticationController extends Controller
{
    protected $authenticationRepository;

    public function __construct(AuthenticationRepositoryInterface $authenticationRepository)
    {
        $this->authenticationRepository = $authenticationRepository;
    }

    /**
     * Registration new account for chosen company
     * Each account able to added more than one company
     * But, one account one company only
     * Duplicated account for same company will be declined
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $request->companyId = Crypt::decryptString($request->companyId);

            $response = $this->authenticationRepository->register($request);

            if (isset($response)) {
                if ($response == 'exist') {
                    DB::rollBack();

                    return $this->errorResponse('Email sudah digunakan', null);
                } else {
                    DB::commit();

                    return $this->successResponse('Registrasi berhasil', $response);
                }
            } else {
                DB::rollBack();

                return $this->errorResponse('Company tidak ditemukan', null);
            }
        } catch (Exception $exception) {
            DB::rollBack();

            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }

    /**
     * Login account
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        try {
            $request->companyId = Crypt::decryptString($request->companyId);

            $response = $this->authenticationRepository->login($request);

            if ($response == 'fail') {
                return $this->errorResponse('Login gagal', 'Username atau password tidak sesuai');
            } else if ($response == 'already login') {
                return $this->errorResponse('Login gagal', 'Akun sedang dalam status login');
            } else if ($response == 'un-registered') {
                return $this->errorResponse('User belum terdaftar di company ini', null);
            } else {
                return $this->successResponse('Login berhasil', $response);
            }
        } catch (Exception $exception) {
            return $this->errorResponse('Company tidak ditemukan', null);
        }
    }

    /**
     * Logout account
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        try {
            $id = Crypt::decryptString($request->id);

            $response = $this->authenticationRepository->logout($id);

            if ($response == 'undefined') {
                return $this->errorResponse('Data user aktif tidak ditemukan', $response);
            } else {
                return $this->successResponse('Logout berhasil', $response);
            }
        } catch (Exception $exception) {
            return $this->errorResponse('Data user aktif tidak ditemukan', null);
        }
    }

    /**
     * User change password
     * @param Request $request
     * @return void
     */
    public function changePassword(Request $request)
    {
        try {
            if (!isset($request->id) ||
                !isset($request->old_password) ||
                !isset($request->new_password)) {
                return $this->errorResponse('Input tidak sesuai', null);
            }

            $request->id = Crypt::decryptString($request->id);

            DB::beginTransaction();

            $response = $this->authenticationRepository->changePassword($request);

            if ($response == 'successfully change password') {
                DB::commit();

                return $this->successResponse('Password berhasil diubah', $response);
            } else if ($response == 'wrong old password') {
                DB::rollBack();

                return $this->errorResponse('Password lama tidak sesuai', null);
            }
        } catch (Exception $exception) {
            DB::rollBack();

            return $this->errorResponse('Data user tidak ditemukan', null);
        }
    }
}

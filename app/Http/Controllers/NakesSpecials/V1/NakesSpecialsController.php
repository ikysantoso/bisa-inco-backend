<?php

namespace App\Http\Controllers\NakesSpecials\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateNakesSpecialsRequest;
use App\Models\NakesSpecials;
use App\Repository\NakesSpecial\V1\NakesSpecialRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class NakesSpecialsController extends Controller
{
    protected $nakesSpecialRepository;

    public function __construct(NakesSpecialRepositoryInterface $nakesSpecialRepository)
    {
        $this->nakesSpecialRepository = $nakesSpecialRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        $response = $this->nakesSpecialRepository->store($request);

        if (isset($response)) {
            return $response;
        } else {
            return 'Already added';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NakesSpecials  $nakesSpecials
     * @return \Illuminate\Http\Response
     */
    public function show(NakesSpecials $nakesSpecials)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NakesSpecials  $nakesSpecials
     * @return \Illuminate\Http\Response
     */
    public function edit(NakesSpecials $nakesSpecials)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateNakesSpecialsRequest  $request
     * @param  \App\Models\NakesSpecials  $nakesSpecials
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNakesSpecialsRequest $request, NakesSpecials $nakesSpecials)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NakesSpecials  $nakesSpecials
     * @return \Illuminate\Http\Response
     */
    public function destroy(NakesSpecials $nakesSpecials)
    {
        //
    }

    public function getByNursingNumber($company_id, $nursing_number)
    {
        return $this->nakesSpecialRepository->getByNursingNumber($company_id, $nursing_number);
    }

    public function getByMasterMedicalCheckupId($medical_checkup_id)
    {
        return $this->nakesSpecialRepository->getByMasterMedicalCheckupId($medical_checkup_id);
    }

    public function getAllByCompany($company_id)
    {
        return $this->nakesSpecialRepository->getAllByCompany(Crypt::decryptString($company_id));
    }

    public function deleteNurse($id)
    {
        $response = $this->nakesSpecialRepository->deleteNurse($id);

        return $this->successResponse('Berhasil menghapus', $response);
    }
}

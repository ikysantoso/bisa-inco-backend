<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => Crypt::encryptString($this->id),
            'employeeId' => Crypt::encryptString($this->employee_id),
            'nik'   => $this->nik,
            'fullname' => $this->fullname,
            'birthdate' => $this->birthdate,
            'division' => $this->division,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'company'   => new CompanyResource($this->company)
        ];
    }
}

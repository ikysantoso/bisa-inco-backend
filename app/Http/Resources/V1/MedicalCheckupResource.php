<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;

class MedicalCheckupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => Crypt::encryptString($this->id),
            'employeeId' => Crypt::encryptString($this->employee_id),
            'height' => $this->height,
            'weight' => $this->weight,
            'sistole' => $this->sistole,
            'diastole' => $this->diastole,
            'bloodSugarLevel' => $this->blood_sugar_level,
            'cholesterolLevel' => $this->cholesterol_level,
            'uricAcidLevel' => $this->uric_acid_level,
            'nursingNumber' => $this->nursing_number,
            'inputtedBySpecialNakes' => $this->inputted_by_special_nakes,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at
        ];
    }
}

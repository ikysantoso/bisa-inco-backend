<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => Crypt::encryptString($this->id),
            'companyId' => Crypt::encryptString($this->company_id),
            'company' => $this->company->company_id,
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'accessStatus' => $this->access_status,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'companyData'   => new CompanyResource($this->company)
        ];
    }
}

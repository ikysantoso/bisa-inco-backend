<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;

class PartnershipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => Crypt::encryptString($this->id),
            'companyId' => $this->company_id,
            'companyName' => $this->company_name,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'employeeTotal' => $this->employees->count(),
            'userData' => $this->users,
            'userTotal' => $this->users->count()
        ];
    }
}

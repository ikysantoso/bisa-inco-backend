<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;

class EmployeeDeleteRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => Crypt::encryptString($this->id),
            'requestId' => $this->request_id,
            'nik' => $this->nik,
            'fullname' => $this->fullname,
            'birthdate' => $this->birthdate,
            'division' => $this->division,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'company'   => new CompanyResource($this->company)
        ];
    }
}

<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;

trait ApiResponser
{
    public function successResponse($message, $data): JsonResponse
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data,
        ], 200);
    }

    public function errorResponse($message, $data): JsonResponse
    {
        return response()->json([
            'success' => false,
            'message' => $message,
            'data' => $data,
        ], 400);
    }

    public function serverErrorResponse(): JsonResponse
    {
        return response()->json([
            'success' => false,
            'message' => 'Mohon maaf request tidak dapat di terima oleh server',
            'data' => null,
        ], 500);
    }
}

<?php

namespace App\Traits;

use GuzzleHttp\Client;
use SendinBlue\Client\Api\TransactionalEmailsApi;
use SendinBlue\Client\ApiException;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Model\CreateSmtpEmail;
use SendinBlue\Client\Model\SendSmtpEmail;

trait MailService
{
    /**
     * Send email service integrated with SendInBlue
     *
     * @throws ApiException
     */
    public function sendMail($subject, $name, $address, $mail_content): CreateSmtpEmail
    {
        // Setup credentials with SendInBlue
        $apiKey = env('SendinBlue_API_KEY');
        $credentials = Configuration::getDefaultConfiguration()->setApiKey('api-key', $apiKey);
        $apiInstance = new TransactionalEmailsApi(new Client(),$credentials);

        // Setup request email that will be sent to SendInBlue
        $sendSmtpEmail = new SendSmtpEmail([
            'subject' => $subject,
            'sender' => ['name' => 'BISA Inco', 'email' => 'no-reply@bisa-inco.com'],
            'to' => [[ 'name' => $name, 'email' => $address]],
            'htmlContent' => $mail_content
        ]);

        // Do sending transaction email with SendInBlue
        return $apiInstance->sendTransacEmail($sendSmtpEmail);
    }

    /**
     * Registration email service for new account
     *
     * @throws ApiException
     */
    public function registrationNewUserForPartnerMail($name, $email, $password, $company): CreateSmtpEmail
    {
        $subject = 'Registrasi Akun Baru';

        $data = array(
            'name'          => $name,
            'email'         => $email,
            'password'      => $password,
            'company'       => $company
        );

        $mail_content = view('mail-registration')->with($data)->render();

        return $this->sendMail($subject, $name, $email, $mail_content);
    }

    /**
     * Reset Password email service
     *
     * @throws ApiException
     */
    public function resetPasswordMail($name, $email, $unique_access): CreateSmtpEmail
    {
        $subject = 'Request Reset Password';

        $data = array(
            'name'                  => $name,
            'unique_access'          => $unique_access
        );

        $mail_content = view('reset-password')->with($data)->render();

        return $this->sendMail($subject, $name, $email, $mail_content);
    }
}

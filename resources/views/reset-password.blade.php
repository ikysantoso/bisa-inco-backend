<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Request Reset Password</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
</head>
<body>
<div class="text" style="padding: 0 2.5em; text-align: center; font-family: 'Lato', sans-serif;">
    <h1>Halo, {{ $name }}</h1>
    <h2>Anda dapat mengubah password pada link berikut ini:</h2>
    <a href="https://bisa-inco.com/reset-password/{{ $unique_access }}">
        https://bisa-inco.com/reset-password/{{ $unique_access }}
    </a>
</div>
</body>
</html>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title>Registrasi Akun Baru</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    </head>
    <body>
        <div class="text" style="padding: 0 2.5em; text-align: center; font-family: 'Lato', sans-serif;">
            <h1>Selamat Datang, {{ $name }}</h1>
            <h2>Anda dapat mengakses BISA Inco pada Perusahaan {{ $company }} dengan akun berikut ini:</h2>
            <h3>Email: {{ $email }}</h3>
            <h3>Password: {{ $password }}</h3>
            <a style="background-color: #04AA6D; border-radius: 12px; color: white; padding: 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; font-weight: bold; margin: 4px 2px; cursor: pointer;" href="https://bisa-inco.com/">Login Sekarang</a>
        </div>
    </body>
</html>

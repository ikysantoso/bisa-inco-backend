<?php

use App\Http\Controllers\Authentication\AuthenticationController;
use App\Http\Controllers\Company\V1\CompanyController;
use App\Http\Controllers\Employee\V1\EmployeeController;
use App\Http\Controllers\Fitness\V1\FitnessController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MedicalCheckup\V1\MedicalCheckupController;
use App\Http\Controllers\NakesSpecials\V1\NakesSpecialsController;
use App\Http\Controllers\Partnership\V1\PartnershipController;
use App\Http\Controllers\ResetPasswordRequest\V1\ResetPasswordRequestController;
use App\Http\Controllers\User\V1\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * API Version 1
 */
Route::prefix('v1')->group(function () {
    /**
     * Authentication API Management
     */
    Route::prefix('auth')->group(function() {
        Route::post('login', [AuthenticationController::class, 'login']);
        Route::post('logout', [AuthenticationController::class, 'logout']);
        Route::post('register', [AuthenticationController::class, 'register']);
        Route::post('change-password', [AuthenticationController::class, 'changePassword']);

        /**
         * Reset Password Request API Management
         */
        Route::prefix('reset-password')->group(function () {
            Route::post('request', [ResetPasswordRequestController::class, 'requestResetPassword']);
            Route::post('validate-unique-access', [ResetPasswordRequestController::class, 'validateUniqueAccess']);
            Route::post('store', [ResetPasswordRequestController::class, 'doResetPassword']);
        });
    });

    /**
     * Company API Management
     */
    Route::prefix('company')->group(function() {
        Route::post('getFitnessEmployee', [CompanyController::class, 'getFitnessEmployeeByCompany']);
        Route::get('company', [CompanyController::class, 'index']);
    });
    Route::resource('company', CompanyController::class);

    /**
     * Employee API Management
     */
    Route::prefix('employee')->group(function() {
        Route::get('nik/search', [EmployeeController::class, 'getByNIK']);
        Route::get('company', [EmployeeController::class, 'getAllEmployeeByCompanyId']);
        Route::get('get-nik/{nik}', [EmployeeController::class, 'getEmployeeLatestFitnessByNIKPublic']);
        Route::get('health-status', [EmployeeController::class, 'getAllEmployeeHealthStatus']);

        Route::prefix('verification')->group(function() {
            Route::prefix('request')->group(function () {
                Route::post('/', [EmployeeController::class, 'requestVerification']);
                Route::get('{id}', [EmployeeController::class, 'getAllVerificationRequestByCompanyId']);
                Route::delete('{id}', [EmployeeController::class, 'deleteEmployeeVerificationRequest']);

                Route::prefix('search')->group(function () {
                    Route::get('company', [EmployeeController::class, 'getEmployeeRequestByCompanyId']);
                    Route::get('nik', [EmployeeController::class, 'getEmployeeRequestByNIK']);
                });
            });

            Route::post('approval', [EmployeeController::class, 'approvalRequest']);
            Route::get('declined', [EmployeeController::class, 'getAllDeclinedVerification']);
        });

        Route::prefix('delete')->group(function() {
            Route::prefix('request')->group(function () {
                Route::post('/', [EmployeeController::class, 'requestDelete']);
                Route::delete('/{id}', [EmployeeController::class, 'deleteEmployeeRequest']);

                Route::prefix('search')->group(function () {
                    Route::get('company', [EmployeeController::class, 'getEmployeeDeleteByCompanyId']);
                    Route::get('nik', [EmployeeController::class, 'getEmployeeRequestDeleteByNIK']);
                });
            });
        });
    });
    Route::resource('employee', EmployeeController::class);

    /**
     * Fitness API Management
     */
    Route::resource('fitness', FitnessController::class);

    /**
     * Medical Checkup API Management
     */
    Route::prefix('medical-checkup')->group(function() {
        Route::get('percentage', [MedicalCheckupController::class, 'getPercentageMedicalCheckup']);
        Route::get('status', [MedicalCheckupController::class, 'getMedicalCheckupByStatus']);
        Route::get('diagnose-total', [MedicalCheckupController::class, 'getDiagnoseTotalMedicalCheckup']);
        Route::get('specific-diagnose', [MedicalCheckupController::class, 'getSpecificDiagnoseFromPercentage']);
        Route::post('import', [MedicalCheckupController::class, 'importData']);

        /**
         * Master Medical Checkup API Management
         */
        Route::prefix('master')->group(function () {
            Route::get('/', [MedicalCheckupController::class, 'getAllMasterMedicalCheckup']);
            Route::delete('{id}', [MedicalCheckupController::class, 'deleteMasterMedicalCheckup']);
            Route::get('company', [MedicalCheckupController::class, 'getAllMasterMedicalCheckupByCompanyId']);
        });
    });
    Route::resource('medical-checkup', MedicalCheckupController::class);

    /**
     * Nakes Special API Management
     */
    Route::get('nakes-special/company/{company_id}', [NakesSpecialsController::class, 'getAllByCompany']);
    Route::get('nakes-special/{master_medical_checkup_id}', [NakesSpecialsController::class, 'getByMasterMedicalCheckupId']);
    Route::get('nakes-special/{company_id}/{nursing_number}', [NakesSpecialsController::class, 'getByNursingNumber']);
    Route::delete('nakes-special/{id}', [NakesSpecialsController::class, 'deleteNurse']);
    Route::resource('nakes-special', NakesSpecialsController::class);
    /**
     * Partnership API Management
     */
    Route::resource('partnership', PartnershipController::class);

    /**
     * User API Management
     */
    Route::prefix('user')->group(function () {
        /**
         * User API Integrated with Company
         */
        Route::prefix('company')->group(function () {
            Route::get('{id}', [UserController::class, 'getCompanyByUser']);
            Route::get('search/{companyId}', [UserController::class, 'getUserByCompanyId']);
            Route::delete('{companyId}', [UserController::class, 'getUserDeleteByCompanyId']);
        });
    });
    Route::resource('user', UserController::class);
});


Route::post('import', [UserController::class, 'importCSV']);
